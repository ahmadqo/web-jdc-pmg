fndTable = {
    searchTable: {}
};

fndTable.initialize = function () {
    if (!variableExists('searchTableId')) {
        return;
    }

    var searchButtonGroup = $('<span class="input-group-btn"></span>');
    var searchButton = $('<button class="btn btn-primary large-button" type="button">Find</button>').appendTo(searchButtonGroup);
    searchButton.on('click', this.refresh);

    this.searchInput = $(`#${searchInputId}`);
    this.searchInput.after(searchButtonGroup);

    $('thead select').on('change', this.refresh);
    $('thead input').on('keypress', function (event) {
        if (event.which === 13) {
            event.preventDefault();
            this.refresh();
        }
    }.bind(this));
    
    this.searchTable = $(`#${searchTableId}`);
    this.searchTable.bootstrapTable();
}

fndTable.refresh = function () {
    fndTable.searchTable.bootstrapTable('refresh');
}

fndTable.searchParameters = function (params) {
    var payload = {
        search: {
            "any": this.searchInput.val()
        }
    };
    if (params.data) {
        if (params.data.limit) {
            payload.sort = params.data.sort;
            payload.order = params.data.order;
            payload.limit = params.data.limit;
            payload.offset = params.data.offset;
        }
    }
    return payload;
}

fndTable.ajax = function (params) {
    if (!variableExists('searchTableUrl')) {
        return;
    }
    var payload = fndTable.searchParameters(params);
    postRequest(window.searchTableUrl, payload, function (result) {
        params.success(result);
    });
}

fndTable.loadForm = function (obj, row, index, mode) {
    if (!variablesExist(['fndFormPage', 'fndFormParameters'])) {
        return;
    }

    let dataRow = this.searchTable.bootstrapTable('getData')[index];
    loadPage(fndFormPage, fndFormParameters(dataRow, mode));
}

fndTable.renderTableActions = function (value, row, index) {
    var objectReturn = ['<div class="btn-group-vertical class="margin-top:1rem;margin-bottom:1rem">'];
    objectReturn.push(
            `<button 
            class="btn btn-primary"
            onclick="fndTable.loadForm(this,'${row}','${index}','view')" 
            title="View">
            View
        </button>`,
            );
    objectReturn.push(
            `<button 
                class="btn btn-primary"
                onclick="fndTable.loadForm(this,'${row}','${index}','edit')" 
                title="Edit">
                Edit
            </button>`,
            );
    if (!row.status) {
        row.status = "";
    }
    objectReturn.push('</div>');
    return objectReturn.join(' ');
};

// =================

fndTable.renderActions = function (value, row, index) {
    var objectReturn = ['<div class="btn-group-vertical class="margin-top:1rem;margin-bottom:1rem">'];
    objectReturn.push(
            `<button 
            class="btn btn-primary"
            onclick="fndTable.loadFormDetail(this,'${row}','${index}','detail')" 
            title="Detail">
            Detail
        </button>`,
            );
    objectReturn.push(
            `<button 
                class="btn btn-primary"
                onclick="fndTable.loadFormEdit(this,'${row}','${index}','edit')" 
                title="Edit">
                Edit
            </button>`,
            );
    objectReturn.push('</div>');
    return objectReturn.join(' ');
};

fndTable.loadFormDetail = function (obj, row, index, mode) {
    if (!variableExists('formPageId')) {
        return;
    }
    const dataRow = $(obj).parents('table').bootstrapTable('getData')[index];
    loadPage(formPageId, {
        commoncode: dataRow.commoncode,
        mode: mode
    });
}

fndTable.loadFormEdit = function (obj, row, index, mode) {
    if (!variableExists('fndFormEditId')) {
        return;
    }
    const dataRow = $(obj).parents('table').bootstrapTable('getData')[index];
    loadPage(fndFormEditId, {
        commoncode: dataRow.commoncode,
        commondesc: dataRow.commondesc,
        displayStatus: dataRow.displayStatus,
        mode: mode
    });
}

fndTable.renderActionsCode = function (value, row, index) {
    var objectReturn = ['<div class="btn-group-vertical class="margin-top:1rem;margin-bottom:1rem">'];
    objectReturn.push(
            `<button 
                class="btn btn-primary"
                onclick="fndTable.loadFormEditCode(this,'${row}','${index}','edit')" 
                title="Edit">
                Edit
            </button>`,
            );
    objectReturn.push('</div>');
    return objectReturn.join(' ');
};

fndTable.loadFormEditCode = function (obj, row, index, mode) {
    if (!variableExists('fndFormEditCode')) {
        return;
    }
    const dataRow = $(obj).parents('table').bootstrapTable('getData')[index];
    loadPage(fndFormEditCode, {
        isAdd: false,
        commoncode: dataRow.commoncode,
        commonkey: dataRow.commonkey,
        mode: mode
    });
}