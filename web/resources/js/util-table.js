var renderTableActions = function (value, row, index) {
    var objectReturn = ['<div class="btn-group-vertical class="margin-top:1rem;margin-bottom:1rem">'];
    objectReturn.push(`<button class="btn btn-primary" onclick="loadForm(this,'${row}','${index}','view')" 
            title="View">View</button>`);
//    if (!row.status) {
//        row.status = "";
//    }
//
//    if (row.projectStatus == "Close") {
//        objectReturn.push(
//                `<button 
//                class="btn btn-primary" 
//                onclick="changeStatusPopUp(this,'${row}','${index}')" 
//                title="Open" 
//            >
//                Open
//            </button>`,
//                );
//    } else if (row.projectStatus == "Open") {
//        objectReturn.push(
//                `<button 
//                class="btn btn-primary" 
//                onclick="changeStatusPopUp(this,'${row}','${index}')" 
//                title="Close" 
//            >
//                Close
//            </button>`,
//                );
//    }



//    if (row.status.toLowerCase() == "draft" && row.isCreator == true) {
        objectReturn.push(
                `<button 
                class="btn btn-primary"
                onclick="loadForm(this,'${row}','${index}','edit')" 
                title="Edit"
            >
                Edit
            </button>`,
                );
        objectReturn.push(
                `<button 
                class="btn btn-primary" 
                onclick="deletePopUp(this,'${row}','${index}')" 
                title="Delete" 
            >
                Delete
            </button>`,
                );
//    }
    objectReturn.push('</div>');
    return objectReturn.join(' ');
};

var tableAjax = function (params) {
    if (!variablesExist(['searchTableUrl', 'searchParameters'])) {
        return;
    }
    var payload = searchParameters(params);
    postRequest(window.searchTableUrl, payload, function (result) {
        params.success(result);
    });
};

var formatNull = function (value, row, index) {
    if (value == null || value == '') {
        return '-';
    }
    return value;
} 