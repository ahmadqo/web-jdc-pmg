//var baseURL = "/jdcdsmun000-rest/rest";

function recursive_menu(menu_obj, array_data, html_string) {
    var menu_childs = new Array();
    html_string += '<ul style="display: none;">';
    $.each(array_data, function (key, value) {
        if (value.vparent == menu_obj.menuId) {
            var menu_child_obj = new Object();
            menu_child_obj.menuId = value.vid;
            menu_child_obj.menuName = value.vtitle;
            if (value.vapplicationId == "null") {
                html_string += '<li class="menu treeview transition">' +
                        '<a>' +
                        '<i class="glyphicon glyphicon-eye-open"></i> <span>' + value.vtitle + '</span>' +
                        '<i class="glyphicon glyphicon-chevron-down icon-menu-expand" style="float:right"></i>' +
                        '</a>';
                html_string = recursive_menu(menu_child_obj, array_data, html_string);
                html_string += '</li>';
            } else {
                html_string += '<li class="menu transition">' +
                        '<a data-formid="' + value.vurl + '">' +
                        '<i class="glyphicon glyphicon-circle-o">' +
                        '</i>' + value.vtitle +
                        '</a>' +
                        '</li>';
            }

            menu_childs.push(menu_child_obj);

        }
    });
    if (menu_childs.length > 0) {
        menu_obj.menuChilds = menu_childs;
    }
    html_string += '</ul>';
    return html_string;
}

function login() {
    var user = $('#username').val();
    var password = $('#password').val();
    if ((user == '') && (password == '')) {
        Swal.fire({
            title: "<i>Required</i>",
            html: "<b>Username & Password</b> is required",
            confirmButtonText: "OK",
        });
        return;
    } else if (user == '') {
        Swal.fire({
            title: "<i>Required</i>",
            html: "<b>Username</b> is required",
            confirmButtonText: "OK",
        });
        return;
    } else if (password == '') {
        Swal.fire({
            title: "<i>Required</i>",
            html: "<b>Password</b> is required",
            confirmButtonText: "OK",
        });
        return;
    }

    $('#alert-error').hide();
    $('#button-login').hide();

    var processResult = function (result) {
        $("#loader-submit").delay(2000).fadeOut();
        window.location = "index.html#/homepage";
//        if (result.status == 1) {
//            $("#loader-submit").show();
//            var message = result.message;
//            localStorage.setItem("role", message.role);
//            localStorage.setItem("npk", message.npk);
//            localStorage.setItem("fullname", capitalize(message.fullname));
//            localStorage.setItem("message", message.message);
//            localStorage.setItem("email", message.email);
//            localStorage.setItem("username", message.username);
//            localStorage.setItem("JDCID", message.JDCID);
//            localStorage.setItem("TKID", message.TKID);
//            localStorage.setItem("isFlag", message.isFlag);
//            if ('' == message.urlPhoto || null == message.urlPhoto) {
//                localStorage.setItem("urlPhoto", `https://ui-avatars.com/api/?length=3&color=000000&background=E9F1F3&name=${localStorage.fullname}`);
//            } else {
//                localStorage.setItem("urlPhoto", message.urlPhoto);
//            }
//
//            if (message.role == 'ADM') {
//                $("#loader-submit").delay(2000).fadeOut();
//                window.location = "index.html#/homepage";
//            } else {
//                $("#loader-submit").delay(2000).fadeOut();
//                window.location = "index.html#/dashboard";
//            }
//        } else {
//            $("#loader-submit").delay(2000).fadeOut();
//            $('#alert-error').show();
//            $('#button-loading').hide();
//            $('#button-login').show();
//        }
    };

//    var realm = 'JDC';
//    var nonce = genRes(32);
//    var uri = '/svc-jdc-dashboard/rest/jdc/dsh000/login';
//    var cnonce = genRes(8);
//    var nc = genNc(8);
//    var qop = 'auth';
//    var res = btoa(realm + ":" + password + ":" + nonce);
//
//    var header = 'Digest username="' + user + '", realm="' + realm
//            + '", nonce="' + nonce + '", uri="' + uri + '", cnonce="'
//            + cnonce + '", nc="' + nc + '", qop="' + qop + '", response="'
//            + res + '"';
//
//    postRequest('/svc-jdc-dashboard/rest/jdc/dsh000/login', {}, processResult, {
//        headers: {
//            Authorization: header
//        },
//        performSuccessAnyway: true
//    });
}

function genNc(b) {
    var c = [], e = "0123456789", a = e.length;
    for (var d = 0; d < b; ++d) {
        c.push(e[Math.random() * a | 0])
    }
    return c.join("")
}
;

function genRes(b) {
    var c = [], e = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789", a = e.length;
    for (var d = 0; d < b; ++d) {
        c.push(e[Math.random() * a | 0])
    }
    return c.join("")
}
;
