pmgTable = {
    searchTable: {}
};

pmgTable.initialize = function () {
    var invalidSymbols = [
        "-",
        "+",
        "e",
    ];
    if (!variableExists('searchTableId')) {
        return;
    }
    this.searchTable = $(`#${searchTableId}`);
    this.searchTable.bootstrapTable();

    $('thead select').on('change', this.refresh);
    $('thead input').on('keypress', function (event) {
        if (event.which === 13) {
            event.preventDefault();
            this.refresh();
        }
    }.bind(this));

    $('thead .numberOnly').on('keydown', function (event) {
        if (invalidSymbols.includes(event.key)) {
            event.preventDefault();
        }
    });
}

pmgTable.refresh = function () {
    pmgTable.searchTable.bootstrapTable('refresh');
//    if (["PRG", "DP"].indexOf(localStorage.projectUserRole) > -1) {
//        $('table .actionTable').hide();
//    }
}

pmgTable.ajax = function (params) {
    if (!variablesExist(['searchTableUrl', 'searchParameters'])) {
        return;
    }
    var payload = searchParameters(params);
    postRequest(window.searchTableUrl, payload, function (result) {
        params.success(result);
    });
}

pmgTable.deleteData = function (dataToDelete) {
    if (!variablesExist(['entityName', 'pmgDeleteUrl'])) {
        return;
    }

    if ('Task' === entityName && dataToDelete.pic.toLowerCase() == localStorage.email.toLowerCase() && dataToDelete.statusTask != 'Completed') {
        let yourTask = localStorage.projectRemainingTask;
        if (yourTask > 0) {
            yourTask = yourTask - 1;
        }
        localStorage.setItem("projectRemainingTask", yourTask);
        document.getElementById('project-remainingTask').innerHTML = "Remaining Task: " + `<span class="badge">${yourTask}</span>`;
    }

    var processResult = function (data) {
        $("#loader-submit").show();
        if (data !== null) {
            if (data.status == "1") {
                setMessage(`${entityName} has been deleted successfully!`);
                this.searchTable.bootstrapTable('refresh');
                stopPreload();
            } else {
                setMessage(`Error in deleting the ${entityName} !`, 'danger');
                stopPreload();
            }
        }
    };
    postRequest(pmgDeleteUrl, dataToDelete, processResult.bind(this));
}

pmgTable.deletePopUp = function (obj, row, index) {
    if (!variablesExist(['entityName', 'pmgDeleteUrl'])) {
        return;
    }

    var dataRow = this.searchTable.bootstrapTable('getData')[index];

    var text = `Are you sure you want do delete this ${entityName}?`;
    var buttons = [{
            text: "Yes",
            action: this.deleteData.bind(this, dataRow)
        }, {
            text: "No"
        }
    ];
    showModalMessage(text, buttons);
}

pmgTable.loadForm = function (obj, row, index, mode) {
    if (!variablesExist(['pmgFormPage', 'pmgFormParameters'])) {
        return;
    }

    let dataRow = this.searchTable.bootstrapTable('getData')[index];
    loadPage(pmgFormPage, pmgFormParameters(dataRow, mode));
}

pmgTable.loadFormInsert = function (obj, row, index, mode) {
    if (!variablesExist(['pmgFormInsertPage', 'pmgFormParameters'])) {
        return;
    }

    let dataRow = this.searchTable.bootstrapTable('getData')[index];
    loadPage(pmgFormInsertPage, pmgFormParameters(dataRow, mode));
}

pmgTable.loadUpdate = function (obj, row, index) {
    if (!variablesExist(['pmgFormPage', 'pmgFormParameters'])) {
        return;
    }

    let dataRow = this.searchTable.bootstrapTable('getData')[index];
    loadPage('JDCFND003', {idTask: dataRow.idTask, isTask: true});
}

pmgTable.renderActions = function (value, row, index) {
    var objectReturn = ['<div class="btn-group-vertical class="margin-top:1rem;margin-bottom:1rem">'];
    objectReturn.push(
            `<button 
            class="btn btn-primary" 
            onclick="pmgTable.loadForm(this,'${row}','${index}','view')" 
            title="View"
        >
            View
        </button>`
            );
    if (!row.status) {
        row.status = "";
    }

    if (row.creator == true) {
        objectReturn.push(
                `<button 
                class="btn btn-primary"
                onclick="pmgTable.loadForm(this,'${row}','${index}','edit')" 
                title="Edit"
            >
                Edit
            </button>`,
                );
        if (row.status.toLowerCase() == "draft") {
            objectReturn.push(
                    `<button 
                class="btn btn-primary" 
                onclick="pmgTable.deletePopUp(this,'${row}','${index}')" 
                title="Delete" 
            >
                Delete
            </button>`,
                    );
        }
    }
    objectReturn.push('</div>');
    return objectReturn.join(' ');
};



pmgTable.renderReadOnlyActions = function (value, row, index) {
    var objectReturn = ['<div class="btn-group-vertical class="margin-top:1rem;margin-bottom:1rem">'];
    objectReturn.push(
            `<button 
            class="btn btn-primary" 
            onclick="pmgTable.loadForm(this,'${row}','${index}','view')" 
            title="View"
        >
            View
        </button>`
            );
    objectReturn.push('</div>');
    return objectReturn.join(' ');
};

pmgTable.renderUpdateActions = function (value, row, index) {
    var objectReturn = ['<div class="btn-group-vertical class="margin-top:1rem;margin-bottom:1rem">'];
    objectReturn.push(
            `<button 
            class="btn btn-primary" 
            onclick="pmgTable.loadForm(this,'${row}','${index}','view')" 
            title="View"
        >
            View
        </button>`
            );
    objectReturn.push(
            `<button 
                class="btn btn-primary"
                onclick="pmgTable.loadForm(this,'${row}','${index}','edit')" 
                title="Edit"
            >
                Edit
            </button>`,
            );
    if (!row.status) {
        row.status = "";
    }
    if (row.status.toLowerCase() == "draft" && row.isCreator == true) {
        objectReturn.push(
                `<button 
                class="btn btn-primary" 
                onclick="pmgTable.deletePopUp(this,'${row}','${index}')" 
                title="Delete" 
            >
                Delete
            </button>`,
                );
    }
    objectReturn.push('</div>');
    return objectReturn.join(' ');
};

pmgTable.renderActionsDocContract = function (value, row, index) {
    var objectReturn = ['<div class="btn-group-horizontal style="margin-top:1rem;margin-bottom:1rem">'];
    objectReturn.push(
            `<a id="jdcpmg000p01-buttonDownload-${index}" class="btn btn-primary btn-sm" href="">View</a>`
            );

    objectReturn.push(
            `<button 
            class="btn btn-primary btn-sm"
            onclick="pmgTable.loadForm(this,'${row}','${index}','edit')" 
            title="Edit"
        >
        Edit
        </button>`
            );
    objectReturn.push('</div>');
    return objectReturn.join(' ');
};
pmgTable.renderActionsDocContractOpr = function (value, row, index) {
    var objectReturn = ['<div class="btn-group-horizontal style="margin-top:1rem;margin-bottom:1rem">'];
    objectReturn.push(
            `<a id="jdcpmg000p02-buttonDownload-${index}" class="btn btn-primary btn-sm" href="">View</a>`
            );

    objectReturn.push(
            `<button 
            class="btn btn-primary btn-sm"
            onclick="pmgTable.loadForm(this,'${row}','${index}','edit')" 
            title="Edit"
        >
        Edit
        </button>`
            );
    objectReturn.push('</div>');
    return objectReturn.join(' ');
};

//BARU
pmgTable.confirmDelete = function (obj, row, index) {
    var dataRow = this.searchTable.bootstrapTable('getData')[index];
    var text = `Are you sure you want do delete this data?`;
    Swal.fire({
        title: 'Are you sure?',
        text: text,
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#E92030',
        cancelButtonColor: '#F4F4F4',
        confirmButtonText: 'Yes',
        cancelButtonText: 'No',
    }).then((result) => {
        if (result.isConfirmed) {
            if (!variablesExist(['pmgDeleteUrl'])) {
                return;
            }
            var processResult = function (data) {
                if (data !== null) {
                    if (data.status == "1") {
                        Swal.fire('Success!', 'Your data has been submited.', 'success');
                        pmgTable.searchTable.bootstrapTable('refresh');
                    } else {
                        Swal.fire('Failed!', 'Error in deleting data.', 'error');
                    }
                }
            };
            var param  = {'id' : dataRow.id};
            var ajaxOptions = {
                type: 'POST',
                url: window.serviceUrl + window.pmgDeleteUrl,
                contentType: 'application/json',
                dataType: 'json',
                async: false,
                data: JSON.stringify(param),
                success: processResult
            };
            $.ajax(ajaxOptions);  
        }
    })
}

pmgTable.renderActionsNew = function (value, row, index) {
    var objectReturn = ['<div class="btn-group-action">'];
    objectReturn.push(
        `<div class="btn-action-table action-view" onclick="pmgTable.loadForm(this,'${row}','${index}','view')" title="View">View</div>`);
    objectReturn.push(
        `<div class="btn-action-table action-edit" onclick="pmgTable.loadForm(this,'${row}','${index}','edit')" title="Edit">Edit</div>`);
    objectReturn.push(
        `<div class="btn-action-table action-delete" onclick="pmgTable.confirmDelete(this,'${row}','${index}')" title="Delete">Delete</div>`);
    objectReturn.push('</div>');
    return objectReturn.join('');
};