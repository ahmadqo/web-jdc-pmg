var pmgUpload = {};

pmgUpload.uploadFile = function (result) {
    $('.content').append("<div id='loader'></div>");
    $('#loader').show();
    $('.btn-validateupload').attr("disabled", "disabled");
//    if (result == undefined) {
//        $('.btn-validateupload').removeAttr("disabled");
//    }
    var processResult = function (result) {
        if (result.status == 1) {
            loadPage(window.returnPage);
        } else {
            $('.btn-validateupload').removeAttr("disabled");
        }
        $('.content').remove("<div id='loader'></div>");
    }
    var payload = $(`#${window.validationTableId}`).bootstrapTable('getData');
    var url = window.url.bulkProcess;
    postRequest(url, payload, processResult);
};

pmgUpload.validateFile = function () {
    $('.alert-dismissable').remove();
    $('#button-upload').hide();
    var data = new FormData();
    var file = $(`#${window.fileFieldId}`)[0].files[0];
    data.append('file', file);
    data.append('projectNumber', localStorage.projectNumber);
    data.append('idTable', localStorage.idTable);
    data.append('projectType', getProjectType());

    var renderStagedData = function (result) {
        if (result.status == 1) {
            $('#button-upload').show();
        } else {
            setMessage(`The uploaded file is invalid`, 'danger');
        }
        var addMessage = datum => {
            datum.message = datum.messageList.map(message => `<li>${message}</li>`).join('')
            datum.message = `<ul>${datum.message}</ul>`;
        };
        if (result.data) {
            if (result.data.forEach) {
                result.data.forEach(addMessage);
            }
        } else {
            return;
        }
        $(`#${window.validationTableId}`).bootstrapTable('load', result.data);
    };

    $.ajax({
        url: window.url.upload,
        data: data,
        cache: false,
        contentType: false,
        processData: false,
        method: 'POST',
        success: renderStagedData
    });
};

pmgUpload.validateFileTask = function () {
    $('.alert-dismissable').remove();
    $('.btn-validateupload').attr("disabled", "disabled");
    $('#button-upload').hide();
    var data = new FormData();
    var file = $(`#${window.fileFieldId}`)[0].files[0];
    data.append('file', file);
    data.append('projectNumber', localStorage.projectNumber);
    data.append('idTable', localStorage.idTable);
    data.append('projectType', getProjectType());
    data.append('projectRole', localStorage.projectUserRole);

    var renderStagedData = function (result) {
        if (result.status == 1) {
            $('#button-upload').show();
        } else {
            setMessage(`The uploaded file is invalid.`, 'danger');
        }

        var addMessage = datum => {
            datum.message = datum.messageList.map(message => `<li>${message}</li>`).join('')
            datum.message = `<ul>${datum.message}</ul>`;
        };

        if (result.data) {
            if (result.data.forEach) {
                result.data.forEach(addMessage);
            }

        } else {
            return;
        }

        $(`#${window.validationTableId}`).bootstrapTable('load', result.data);
        $('.btn-validateupload').removeAttr("disabled");
    };

    $.ajax({
        url: window.url.upload,
        data: data,
        cache: false,
        contentType: false,
        processData: false,
        method: 'POST',
        success: renderStagedData
    });
};

pmgUpload.readyFunction = function () {
    $(`#${window.validationTableId}`).bootstrapTable({data: []});
};
