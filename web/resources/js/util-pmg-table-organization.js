pmgTableOrganization = {
    searchTable: {}
};

pmgTableOrganization.initialize = function () {
    var invalidSysmbols = [
        "-",
        "+",
        "e"
    ];
    if (!variablesExist('searchTableIdOrganization')) {
        return;
    }
    this.searchTable = $(`#${searchTableIdOrganization}`);
    this.searchTable.bootstrapTable();

    $('this select.organization').on('change', this.refresh);
    $('this input.organization').on('keypress', function (event) {
        if (event.which === 13) {
            event.preventDefault();
            this.refresh();
        }
    }.bind(this));

    $('thead .numerorganization').on('keydown', function (event) {
        if (invalidSysmbols.includes(event.key)) {
            event.preventDefault();
        }
    });
}

pmgTableOrganization.refresh = function () {
    pmgTableOrganization.searchTable.bootstrapTable('refresh');
}

pmgTableOrganization.ajax = function (params) {
    if (!variablesExist(['searchTableUrlOrganization', 'searchParametersOrganization'])) {
        return;
    }
    var payload = searchParametersOrganization(params);
    postRequest(window.searchTableUrlOrganization, payload, function (result) {
        params.success(result);
    });
}

pmgTableOrganization.loadForm = function (obj, row, index, mode) {
    if (!variablesExist(['pmgFormPageOrganization', 'pmgFormParametersOrganization'])) {
        return;
    }
    let dataRow = this.searchTable.bootstrapTable('getData')[index];
    loadPage(pmgFormPageOrganization, pmgFormParametersOrganization(dataRow, mode));
}

pmgTableOrganization.renderUpdateActions = function (value, row, index) {
    var objectReturn = ['<div class="btn-group-vertical class="margin-top:1rem;margin-bottom:1rem">'];
    objectReturn.push(
        `<button 
            class="btn btn-primary" 
            onclick="pmgTableOrganization.loadForm(this,'${row}','${index}','view')" 
            title="View"
        >
            View
        </button>`
    );
    objectReturn.push(
        `<button 
            class="btn btn-primary"
            onclick="pmgTableOrganization.loadForm(this,'${row}','${index}','edit')" 
            title="Edit"
        >
            Edit
        </button>`,
    );
    objectReturn.push('</div>');
    return objectReturn.join(' ');
}