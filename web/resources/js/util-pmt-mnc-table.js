var pmtTable_pica = {};
var pmtTable_mom = {};
var pmtForm = {}

//// --------------------GENERIC FUNCTION-------------
pmtForm.validateForm = function () {
    var validated = validateForm(`#${formId}`);
    if (validated) {
        return true;
    } else {
        var message = 'Please fill all the required fields';
        var buttons = [{
                text: 'Ok'
            }, ];
        showModalMessage(message, buttons);
        return false;
    }
}

pmtForm.downloadReport = function (targetId, mode, pnumber, start, end) {
    if (!variableExists('downloadReportUrl')) {
        return;
    }
//    console.log(pnumber);
    var url = "";
    var payload = {}

    if (mode == 'pica') {
        url = "/svc-jdc-project-management-tools/rest/jdc/pmt002/download-report-pica";
        payload = {
            "pnumber": pnumber,
            "picaid": targetId,
            "start": start,
            "end": end,
        }
    } else if (mode == 'mom') {
        url = "/svc-jdc-project-management-tools/rest/jdc/pmt003/download-mom";
        payload = {
            "pnumber": localStorage.projectNumber,
            "idMom": targetId,
        }
    }

//    console.log(payload);
    postRequest(url, payload, function (result) {
//        console.log(result);
        var download = result.data[0];
//        console.log(download);
        window.open(download, '_blank');
    });

}

//// ----------------------- PICA --------------------

pmtTable_pica.initialize = function () {
    this.searchTable = $(`#${picaTableId}`);
    this.searchTable.bootstrapTable();
}


pmtTable_pica.loadFormEdit = function (obj, row, index, mode) {
//    console.log('HAI');
    if (!variablesExist(['pmtFormEditPage', 'pmtFormEditParameters'])) {
        return;
    }

    let dataRow = this.searchTable.bootstrapTable('getData')[index];
//    console.log(dataRow);
//    console.log(mode);
    loadPage(pmtFormEditPage, pmtFormEditParameters(dataRow, mode));
}

pmtTable_pica.closePica = function (obj, row, index, status) {
//    console.log('HAI');
    let dataRow = this.searchTable.bootstrapTable('getData')[index];
//    console.log(dataRow);
    approvePica(dataRow, status);
}

pmtTable_pica.refresh = function () {
    pmtTable_pica.searchTable.bootstrapTable('refresh');
};

pmtTable_pica.renderActionsPica = function (value, row, index) {
    // var disabled = (row.status == '004' ? 'disabled' : '');
    var objectReturn = ['<div class="btn-group-vertical" >'];
    if (localStorage.projectUserRole == 'PM') {
        objectReturn.push(
                `<button
            type="button"
            class="btn btn-info"
            onclick="pmtForm.downloadReport('${row.picaid}','pica', '${row.pnumber}', '${row.start}', '${row.end}')"
            >
            Download PICA
        </button>
        <button
            type="button"
            class="btn btn-primary"
            onclick="pmtTable_pica.loadFormEdit(this,'${row}','${index}','${row.status == 'Rejected' ? 'edit' : 'view'}')"
            >
            ${row.status == 'Rejected' ? 'Edit' : 'View'}
        </button>`,
                );
    }
    if (localStorage.projectUserRole == 'PRG') {
        objectReturn.push(
                `
        <button
            type="button"
            class="btn btn-primary"
            onclick="pmtTable_pica.loadFormEdit(this,'${row}','${index}','view')"
            >
            View
        </button>
         <button
            type="button"
            class="btn btn-primary"
            onclick="pmtTable_pica.closePica(this,'${row}','${index}', 'approved')"
            ${row.status != 'Waiting for Approval' ? 'disabled' : ''}
            >
            Approve
        </button>
        <button
            type="button"
            class="btn btn-danger"
            onclick="pmtTable_pica.closePica(this,'${row}','${index}', 'rejected')"
            ${row.status != 'Waiting for Approval' ? 'disabled' : ''}
            >
            Reject
        </button>
        `,
                );
    }
    if (localStorage.projectUserRole == 'EPM') {
        objectReturn.push(
                `<button
                type="button"
                class="btn btn-primary"
                onclick="pmtTable_pica.loadFormEdit(this,'${row}','${index}','view')"
                >
                View
            </button>`,
                );
    }
    objectReturn.push('</div>');
    return objectReturn.join('  ');
};

pmtTable_pica.renderTextareaPotential = function (value, row, index) {
    if (row.potential == null) {
        var rowPotential = "";
    } else {
        var rowPotential = row.potential;
    }
    var objectReturn = ['<div class="btn-group-vertical class="margin-top:1rem;margin-bottom:1rem">'];
    objectReturn.push(`<textarea style="color:#000;resize: none;" id="potential-${row.issueid}" name="${row.issueid}" rows="8" cols="40">${rowPotential}</textarea>`, );
    objectReturn.push('</div>');
    return objectReturn.join('  ');
};

pmtTable_pica.renderTextareaPreventive = function (value, row, index) {
    if (row.preventive == null) {
        var rowPreventive = "";
    } else {
        var rowPreventive = row.preventive;
    }
    var objectReturn = ['<div class="btn-group-vertical class="margin-top:1rem;margin-bottom:1rem">'];
    objectReturn.push(`<textarea style="color:#000;resize: none;" id="preventive-${row.issueid}" name="${row.issueid}" rows="8" cols="40">${rowPreventive}</textarea>`, );
    objectReturn.push('</div>');
    return objectReturn.join('  ');
};

pmtTable_pica.renderCreatedDate = function (value, row, index) {
    var objectReturn = ['<div class="btn-group-vertical class="margin-top:1rem;margin-bottom:1rem">'];
    // Create a new JavaScript Date object based on the timestamp
    var date = new Date(row.dcrea);
    // Date part from the timestamp
    var datex = "0" + date.getDate();
    // Month part from the timestamp
    var monthx = "0" + date.getMonth();
    // FullYear part from the timestamp
    var yearx = date.getFullYear();
    // Hours part from the timestamp
    var hours = "0" + date.getHours();
    // Minutes part from the timestamp
    var minutes = "0" + date.getMinutes();
    // Seconds part from the timestamp
    var seconds = "0" + date.getSeconds();

    // Will display date and time in 01-01-2019 08:03:03 format
    var formattedTime = `${datex.substr(-2)}-${monthx.substr(-2)}-${yearx} ${hours.substr(-2)}:${minutes.substr(-2)}:${seconds.substr(-2)}`;
    // hours + ':' + minutes.substr(-2) + ':' + seconds.substr(-2);

    objectReturn.push(formattedTime, );
    objectReturn.push('</div>');
    return objectReturn.join('  ');
};

pmtTable_pica.renderOverallStatus = function (value, row, index) {
    var objectReturn = ['<div class="btn-group-vertical class="margin-top:1rem;margin-bottom:1rem">'];
    if (row.overall == '001') {
        objectReturn.push(`<div class="dot btn-success"></div>`, );
    } else if (row.overall == '002') {
        objectReturn.push(`<div class="dot btn-warning"></div>`, );
    } else if (row.overall == '003') {
        objectReturn.push(`<div class="dot btn-danger"></div>`, );
    }
    objectReturn.push('</div>');
    return objectReturn.join('  ');
};

pmtTable_pica.searchParameters = function (params) {
    var payload = {
        search: {
            "=pnumber": localStorage.projectNumber,
        }
    };
    if (params.data) {
        if (params.data.limit) {
            payload.sort = params.data.sort;
            payload.order = params.data.order;
            payload.limit = params.data.limit;
            payload.offset = params.data.offset;
        }
    }

    return payload;
}

pmtTable_pica.ajax = function (params) {
    if (!variableExists('searchTableUrl')) {
        return;
    }
    var payload = pmtTable_pica.searchParameters(params);
//    console.log(payload);
    postRequest(window.searchTableUrl, payload, function (result) {
//        console.log(result);
        params.success(result);
    });

};

//////// --------------------------- MOM

pmtTable_mom.initialize = function () {
    this.searchTable = $(`#${momTableId}`);
    this.searchTable.bootstrapTable();
}

var pmtUpload = {};

pmtTable_mom.validateFile = function () {
    $('.alert-dismissable').remove();

    $('#button-upload').hide();
    var data = new FormData();
    var file = $(`#${window.fileFieldId}`)[0].files[0];
    data.append('file', file);
    data.append('projectNumber', localStorage.projectNumber);
    data.append('idTable', pageParameters.docassignid);

    var renderStagedData = function (result) {
        if (result.status == 1) {
            //$('#button-upload').show();
            var text = `File has been uploaded`;
            var buttons = [{
                    text: "Ok"
                }];
            showModalMessage(text, buttons);
        } else {
            var text = `File format not supported`;
            var buttons = [{
                    text: "Ok"
                }];
            showModalMessage(text, buttons);
        }
        var addMessage = datum => {
            datum.message = datum.messageList.map(message => `<li>${message}</li>`).join('')
            datum.message = `<ul>${datum.message}</ul>`;
        };
        if (result.data) {
            if (result.data.forEach) {
                result.data.forEach(addMessage);
            }
        } else {
            return;
        }
        $(`#${window.validationTableId}`).bootstrapTable('load', result.data);
    };

    $.ajax({
        url: url.upload,
        data: data,
        cache: false,
        contentType: false,
        processData: false,
        method: 'POST',
        success: renderStagedData
    });
};

pmtTable_mom.renderFirstColMom = function (value, row, index) {
    var objectReturn = ['<div class="btn-group-vertical" style="margin-top:10px;margin-bottom:10px;text-align:left;">'];
    objectReturn.push(`<h5 style="display:inline-block;width:20px;">${index + 1}</h5> <h5 style="text-decoration: underline;display:inline-block;">${row.title}</h5><br/>`, );
    objectReturn.push(`<h5 style="width:20px;display:inline-block"></h5><div style="display:inline-block"><span>${row.tanggal} ${row.start} - ${row.end} | ${row.place} </span></div><br/>`, );
    objectReturn.push(`<h5 style="width:20px;display:inline-block"></h5><div style="display:inline-block"><span><b>${row.minutetaker}</b>,${row.attendees}</span></div><br/>`, );
    // objectReturn.push(`<textarea style="color:#000;resize: none;" id="preventive-${row.issueid}" name="${row.issueid}" rows="8" cols="40">${row.preventive}</textarea>`,);
    objectReturn.push('</div>');
    return objectReturn.join('  ');
};

pmtTable_mom.renderCreatedDate = function (value, row, index) {

    var objectReturn = ['<div class="btn-group-vertical" style="margin-top:10px;margin-bottom:10px;text-align:left;">'];
    objectReturn.push(`<p class="text-success">${row.status}</p>`, );
    objectReturn.push(`<p>${row.datecrea}</p>`, );
    objectReturn.push('</div>');
    return objectReturn.join('  ');
};

pmtTable_mom.loadFormEdit = function (obj, row, index, mode) {
    if (!variablesExist(['pmtFormEditPage', 'pmtFormEditParameters'])) {
        return;
    }

    let dataRow = this.searchTable.bootstrapTable('getData')[index];
//    console.log(dataRow);
    loadPage(pmtFormEditPage, pmtFormEditParameters(dataRow, mode));
}

pmtTable_mom.renderActions = function (value, row, index) {
    var objectReturn = ['<div class="btn-group-vertical class="margin-top:1rem;margin-bottom:1rem">'];
    objectReturn.push(
            `<button
            type="button"
            class="btn btn-info"
            onclick="pmtForm.downloadReport('${row.idMom}', 'mom', '${row.pnumber}', '', '')"
            title="Download"
            >
            Download MOM
        </button>
        <button
            type="button"
            class="btn btn-primary"
            onclick="pmtTable_mom.loadFormEdit(this,'${row}','${index}','edit')"
            title="Edit"
            >
            Edit
        </button>`
            );
    objectReturn.push('</div>');
    return objectReturn.join('  ');
};

pmtTable_mom.searchParameters = function (params) {
    var payload = {
        search: {
            "=pnumber": localStorage.projectNumber,
        }
    };
    if (params.data) {
        if (params.data.limit) {
            payload.sort = params.data.sort;
            payload.order = params.data.order;
            payload.limit = params.data.limit;
            payload.offset = params.data.offset;
        }
    }

    return payload;
}

pmtTable_mom.ajax = function (params) {
    if (!variableExists('searchTableUrl')) {
        return;
    }
    var payload = pmtTable_mom.searchParameters(params);
//    console.log(payload);
    postRequest(window.searchTableUrl, payload, function (result) {
//        console.log(result);
        params.success(result);
    });

};


//////// --------------------------- NEW METHOD
