var uamTable = {};

uamTable.initialize = function () {
    if (!variablesExist(['searchTableId', 'searchInputId'])) {
        return;
    }

    var searchButtonGroup = $('<span class="input-group-btn"></span>');
    var searchButton = $('<button class="btn btn-primary large-button" type="button">Find</button>').appendTo(searchButtonGroup);
    searchButton.on('click', this.refresh);

    this.searchInput = $(`#${searchInputId}`);
    this.searchInput.after(searchButtonGroup);
    this.searchInput.on('keypress', function (event) {
        if (event.which === 13) {
            event.preventDefault();
            this.refresh();
        }
    }.bind(this));

    this.searchTable = $(`#${searchTableId}`);
    this.searchTable.bootstrapTable();
}

uamTable.refresh = function () {
    uamTable.searchTable.bootstrapTable('refresh');
};

uamTable.renderActions = function (value, row, index) {
    var objectReturn = ['<div class="btn-group-vertical class="margin-top:1rem;margin-bottom:1rem">'];



    objectReturn.push(
            `<button 
            type="button"
            class="btn btn-primary" 
            onclick="uamTable.loadForm(this,'${row}','${index}','view')">
            View
        </button>`,
            );

    objectReturn.push(
            `<button 
            type="button"
            class="btn btn-primary" 
            onclick="uamTable.loadForm(this,'${row}','${index}','edit')">
            Edit
        </button>`
            );
    objectReturn.push('</div>');
    return objectReturn.join('  ');
};

uamTable.searchParameters = function (params) {
    var payload = {
        search: {
            "any": this.searchInput.val()
        }
    };
    if (params.data) {
        if (params.data.limit) {
            payload.sort = params.data.sort;
            payload.order = params.data.order;
            payload.limit = params.data.limit;
            payload.offset = params.data.offset;
        }
    }

    return payload;
}

uamTable.loadForm = function (obj, row, index, mode) {
    if (!variableExists('formPageId')) {
        return;
    }
    var dataRow = this.searchTable.bootstrapTable('getData')[index];
    loadPage(formPageId, {
        vid: dataRow.vid,
        mode: mode
    });
}

uamTable.performProcedure = function (procedureName, selectedUsers) {
    if (!variableExists('procedureUrls')) {
        return;
    }
    var url = procedureUrls[procedureName];
    var payload = selectedUsers.map(function (selectedUser) {
        return {
            vid: selectedUser.vid
        }
    });
    postRequest(url, payload, uamTable.refresh);
};

uamTable.ajax = function (params) {
    if (!variableExists('searchTableUrl')) {
        return;
    }
    var payload = uamTable.searchParameters(params);
    postRequest(window.searchTableUrl, payload, function (result) {
        params.success(result);
    });
};