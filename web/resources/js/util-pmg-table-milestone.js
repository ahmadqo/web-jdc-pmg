pmgTableMile = {
    searchTable: {}
};

pmgTableMile.initialize = function () {
    if (!variableExists('searchTableIdMile')) {
        return;
    }
    this.searchTable = $(`#${searchTableIdMile}`);
    this.searchTable.bootstrapTable();

    $('thead select.mile').on('change', this.refresh);
    $('thead input.mile').on('keypress', function (event) {
        if (event.which === 13) {
            event.preventDefault();
            this.refresh();
        }
    }.bind(this));
}

pmgTableMile.refresh = function () {
    pmgTableMile.searchTable.bootstrapTable('refresh');
    if (localStorage.projectUserRole == "PRG") {
        $('table .actionTable').hide();
    }
}

pmgTableMile.ajax = function (params) {
    if (!variablesExist(['searchTableUrlMile', 'searchParametersMile'])) {
        return;
    }
    var payload = searchParametersMile(params);
    postRequest(window.searchTableUrlMile, payload, function (result) {
        params.success(result);
    });
}

pmgTableMile.deleteData = function (dataToDelete) {
    if (!variablesExist(['entityNameMile', 'pmgDeleteUrlMile'])) {
        return;
    }

    var processResult = function (data) {
        $("#loader-submit").show();
        if (data !== null) {
            if (data.status == "1") {
                setMessage(`${entityNameMile} has been deleted successfully!`, null, `mile`);
                this.searchTable.bootstrapTable('refresh');
                stopPreload();
            } else {
                setMessage(`Error in deleting the ${entityNameMile} !`, 'danger', `mile`);
                stopPreload();
            }
        }
    };
    postRequest(pmgDeleteUrlMile, dataToDelete, processResult.bind(this));
}

pmgTableMile.deletePopUp = function (obj, row, index) {
    if (!variablesExist(['entityNameMile', 'pmgDeleteUrlMile'])) {
        return;
    }
    var dataRow = this.searchTable.bootstrapTable('getData')[index];

    var text = `Are you sure you want do delete this ${entityNameMile}?`;
    var buttons = [{
            text: "Yes",
            action: this.deleteData.bind(this, dataRow)
        }, {
            text: "No"
        }
    ];
    showModalMessage(text, buttons);
}

pmgTableMile.loadForm = function (obj, row, index, mode) {
    if (!variablesExist(['pmgFormPageMile', 'pmgFormParametersMile'])) {
        return;
    }

    let dataRow = this.searchTable.bootstrapTable('getData')[index];
    loadPage(pmgFormPageMile, pmgFormParametersMile(dataRow, mode));
}

pmgTableMile.renderActions = function (value, row, index) {
    var objectReturn = ['<div class="btn-group-vertical class="margin-top:1rem;margin-bottom:1rem">'];
    objectReturn.push(
            `<button 
            class="btn btn-primary" 
            onclick="pmgTableMile.loadForm(this,'${row}','${index}','view')" 
            title="View"
        >
            View
        </button>`
            );
    if (!row.status) {
        row.status = "";
    }

    if (row.isCreator == true) {
        objectReturn.push(
                `<button class="btn btn-primary"
                onclick="pmgTableMile.loadForm(this,'${row}','${index}','edit')" 
                title="Edit">
                Edit
            </button>`,
            //                );
            //        if (row.status.toLowerCase() == "draft") {
            //            objectReturn.push(
            //                `<button class="btn btn-primary" 
            //                onclick="pmgTableMile.deletePopUp(this,'${row}','${index}')" 
            //                title="Delete">
            //                Delete
            //            </button>`,
        );
    }
//    }
    objectReturn.push('</div>');
    return objectReturn.join(' ');
};

pmgTableMile.renderReadOnlyActions = function (value, row, index) {
    var objectReturn = ['<div class="btn-group-vertical class="margin-top:1rem;margin-bottom:1rem">'];
    objectReturn.push(
            `<button 
            class="btn btn-primary" 
            onclick="pmgTableMile.loadForm(this,'${row}','${index}','view')" 
            title="View"
        >
            View
        </button>`
            );
    objectReturn.push('</div>');
    return objectReturn.join(' ');
};