var menu = [];

const renderMenuGroupElement = function (menuGroup) {
    let menuGroupElement = $(`<li class="treeview"></li>`);
    let menuGroupLink = $(`<a href="#"></a>`).appendTo(menuGroupElement);

    if (menuGroup.icon == null || menuGroup.icon == '') {
        menuGroupLink.append(`<i class="menu-icon fa fa-laptop"></i>`);
    } else {
        menuGroupLink.append(`<img class="menu-icon" src="${menuGroup.icon}"/>`);
    }

    menuGroupLink.append(`<span>${menuGroup.title}</span>`);

    if (menuGroup.url == null || menuGroup.url == 'null' || menuGroup.url == '') {
        //Do nothing
    } else {
        menuGroupLink.on('click', loadPage.bind(null, menuGroup.url, null));
    }
    
    let menuChildrenElement = $('<ul class="treeview-menu"></ul>').appendTo(menuGroupElement);

    let menuChildren = menu.filter(menuItem => menuItem.parent == menuGroup.vid);
    
    if (menuGroup.vid == "DSH"){
        loadMaterializeMenu();
    }

    menuChildren.forEach(menuChild => {
        let menuChildListItem = $('<li></li>').appendTo(menuChildrenElement);
        let menuChildAnchor = $('<a class="menu-anchor" href="#"></a>').appendTo(menuChildListItem);

        if (menuChild.icon == null || menuChild.icon == '') {
            menuChildAnchor.append(`<i class="menu-icon fa fa-circle-o"></i>`);
        } else {
            menuChildAnchor.append(`<img class="menu-icon" src="${menuChild.icon}"/>`);
        }

        menuChildAnchor.append(menuChild.title);

        menuChildAnchor.on('click', function () {
            $('.menu-anchor').removeClass('active');
            $(this).addClass('active');
            loadPage(menuChild.vid)
        });
    });

    return menuGroupElement;
};

const renderGroupedMenu = function () {
    let menuContainer = $('#menu');
    let menuGroups = menu.filter(menuItem => menuItem.parent == null || menuItem.parent == '');
    let menuGroupElements = menuGroups.map(menuGroup => renderMenuGroupElement(menuGroup));
    menuContainer.empty();
    menuGroupElements.forEach(menuGroupElement => menuContainer.append(menuGroupElement));
}

const renderFlatMenu = function (searchKeyword) {
    let menuContainer = $('#menu');
    let filteredMenu = menu.filter(function (menuItem) {
        if (menuItem.parent == null || menuItem.parent == 'null' || menuItem.parent == '') {
            return false;
        }
        return menuItem.parent != null && menuItem.title.toLowerCase().indexOf(searchKeyword) > -1
    });
    menuContainer.empty();
    filteredMenu.forEach(menuItem => {
        let menuItemElement = $(
                `<li><a href="#">
                <i class="fa fa-circle-o"></i>${menuItem.title}
            </a><\li>`
                );
        menuItemElement.on('click', function () {
            loadPage(menuChild.vid)
        })
        menuContainer.append(menuItemElement);
    });
};

const renderMenu = function () {
    var searchKeyword = $('#menu-search').val();
    if (searchKeyword == null || searchKeyword == "") {
        renderGroupedMenu();
    } else {
        renderFlatMenu(searchKeyword);
    }
};

const loadMenu = function (role) {
    if (!role) {
        role = localStorage.projectUserRole;
        if (!role){
            role = localStorage.role;
        }
    }

    if (window.readOnlyMode) {
        return;
    }
    
    var processResult = function (result) {
        if (!result.data) {
            console.log("Can't find menu data in: ", result);
            return;
        }
        menu = result.data;
        renderMenu();
        $("#menu-search").on('keyup', renderMenu);
    }
    // postRequest("/svc-jdc-dashboard/rest/jdc/dsh000/menu-role", role, processResult, {async: true});
};
