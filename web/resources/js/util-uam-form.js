uamForm = {};

uamForm.confirmSave = function () {
    if (!variablesExist(['formId', 'entityName'])) {
        return;
    }

    var message = "";
    var buttons = [];

    var validated = validateForm(`#${formId}`);
    if (validated) {
        var message = `Are you sure you want to submit the ${entityName} ?`;
        var buttons = [
            {text: 'Yes', action: uamForm.saveData},
            {text: 'No'}
        ];
    } else {
        var message = 'Please fill all the required fields';
        var buttons = [
            {text: 'Ok'},
        ];
    }
    showModalMessage(message, buttons);
};

uamForm.saveData = function () {
    if (!variablesExist(['fieldPrefix', 'formFields', 'idField', 'returnPage', 'url', 'entityName'])) {
        return;
    }
    var formObject = createFormObject(fieldPrefix, formFields, idField)
    if (formObject[idField]) {
        var saveUrl = url.edit;
    } else {
        var saveUrl = url.save;
    }
    var success = function (result) {
        if (result) {
            if (result.status == 1) {
                loadPage(returnPage, {id: result.data[0].id});
            } else {
                setMessage(`Saving ${entityName} Failed!`,'danger');
            }
        }
    };
    postRequest(saveUrl, formObject, success);
};

uamForm.processPageParameters = function () {
    if (!variablesExist(['entityName'])) {
        return;
    }
    if (pageParameters) {
        if (pageParameters.mode == 'view') {
            $('input, textarea, select').prop('disabled', true);
            $('title').text(titleText);
        } else {
            elementExists(".save-button");
            $(".save-button").show();
        }

        if (pageParameters.mode) {
            var titleText = pageParameters.mode.charAt(0).toUpperCase() + pageParameters.mode.slice(1);
        } else {
            var titleText = 'Add';
        }

        titleText += ' ' + entityName;
        $('title').text(titleText);

        elementExists("#title");
        $('#title').text(titleText);
        uamForm.getData(pageParameters);
    }
};

uamForm.getData = function (parameters) {
    if (!variablesExist(['url', 'fieldPrefix'])) {
        return;
    }

    if (pageParameters.mode != 'view' && pageParameters.mode != 'edit') {
        return;
    }
//    delete pageParameters.mode;

    parameters = {search: parameters};
    postRequest(url.get, parameters, function (result) {
        if (result.data) {
            result.data = result.data.filter(datum => datum.vid == pageParameters.vid);
        }
        fillFormFields(result, fieldPrefix)
    });
}