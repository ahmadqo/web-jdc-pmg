pmgTableTop = {
    searchTable: {}
};

pmgTableTop.initialize = function () {
    var invalidSymbols = [
        "-",
        "+",
        "e",
    ];
    if (!variableExists('searchTableIdTop')) {
        return;
    }
    this.searchTable = $(`#${searchTableIdTop}`);
    this.searchTable.bootstrapTable();

    $('thead select.top').on('change', this.refresh);
    $('thead input.top').on('keypress', function (event) {
        if (event.which === 13) {
            event.preventDefault();
            this.refresh();
        }
    }.bind(this));

    $('thead .numbertop').on('keydown', function (event) {
        if (invalidSymbols.includes(event.key)) {
            event.preventDefault();
        }
    });
}

pmgTableTop.refresh = function () {
    pmgTableTop.searchTable.bootstrapTable('refresh');
    if (["PRG", "DP"].indexOf(localStorage.projectUserRole) > -1) {
        $('table .actionTable').hide();
    }
}

pmgTableTop.ajax = function (params) {
    if (!variablesExist(['searchTableUrlTop', 'searchParametersTop'])) {
        return;
    }
    var payload = searchParametersTop(params);
    postRequest(window.searchTableUrlTop, payload, function (result) {
        params.success(result);
    });
}

pmgTableTop.deleteData = function (dataToDelete) {
    if (!variablesExist(['entityNameTop', 'pmgDeleteUrlTop'])) {
        return;
    }

    var processResult = function (data) {
        if (data !== null) {
            if (data.status == "1") {
                setMessage(`${entityNameTop} has been deleted successfully!`, null, 'top');
                this.searchTable.bootstrapTable('refresh');
            } else {
                setMessage(`Error in deleting the ${entityNameTop} !`, 'danger', 'top');
            }
        }
    };
    postRequest(pmgDeleteUrlTop, dataToDelete, processResult.bind(this));
}

pmgTableTop.deletePopUp = function (obj, row, index) {
    if (!variablesExist(['entityNameTop', 'pmgDeleteUrlTop'])) {
        return;
    }

    var dataRow = this.searchTable.bootstrapTable('getData')[index];

    var text = `Are you sure you want do delete this ${entityNameTop}?`;
    var buttons = [{
            text: "No"
        }, {
            text: "Yes",
            action: this.deleteData.bind(this, dataRow)
        }
    ];
    showModalMessage(text, buttons);
}

pmgTableTop.loadForm = function (obj, row, index, mode) {
    if (!variablesExist(['pmgFormPageTop', 'pmgFormParametersTop'])) {
        return;
    }

    let dataRow = this.searchTable.bootstrapTable('getData')[index];
    loadPage(pmgFormPageTop, pmgFormParametersTop(dataRow, mode));
}

pmgTableTop.renderActions = function (value, row, index) {
    var objectReturn = ['<div class="btn-group-vertical class="margin-top:1rem;margin-bottom:1rem">'];
    objectReturn.push(
            `<button 
            class="btn btn-primary" 
            onclick="pmgTableTop.loadForm(this,'${row}','${index}','view')" 
            title="View">
            View
        </button>`
            );
    if (!row.status) {
        row.status = "";
    }

    if (row.isCreator == true) {
        objectReturn.push(
                `<button 
                class="btn btn-primary"
                onclick="pmgTableTop.loadForm(this,'${row}','${index}','edit')" 
                title="Edit">
                Edit
            </button>`,
                );
        if (row.status.toLowerCase() == "draft") {
            objectReturn.push(
                    `<button 
                class="btn btn-primary" 
                onclick="pmgTableTop.deletePopUp(this,'${row}','${index}')" 
                title="Delete">
                Delete
            </button>`,
                    );
        }
    }
    objectReturn.push('</div>');
    return objectReturn.join(' ');
};