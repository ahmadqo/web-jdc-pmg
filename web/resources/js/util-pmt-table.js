/*initialiazation table*/
var pmtTable_prepare = {};
var pmtTable_define = {};
var pmtTable_develop = {};
var pmtTable_design = {};
var pmtTable_deploy = {};
var pmtTable_complete = {};
var pmtTable_control = {};
var pmtTable_other = {};
var pmtTable_agreement = {};
var pmtTable_detail = {};
var pmtTable_eacrevenue = {};
var pmtTable_eaccost = {};
var pmtTable_fintrack = {};
var pmtTable_fintrack_actual = {};
var pmtTable_fintrack_budget = {};
var pmtTable_fintrack_commit = {};
var pmtTable = {};
var pmtUpload = {};

var pmtProjectMember = [];
var pmtProjectMemberEmail = [];
var currentDocAssign = '';

/*create table*/
function initTable(object, id, name, searchTableUrl) {
    object.searchTable = $(`#${id}`);
    object.searchTable.bootstrapTable();

    object.loadForm = function (obj, row, index, mode) {
        if (!variableExists('formPageId')) {
            return;
        }
        var dataRow = this.searchTable.bootstrapTable('getData')[index];
        if (dataRow != null || dataRow != undefined) {
            loadPage(detailPageId, {
                docassignid: dataRow.docassignid,
                mode: mode
            });
        }
    }

    object.loadFormNote = function (obj, row, index, mode) {
        if (!variableExists('formPageId')) {
            return;
        }
        var dataRow = this.searchTable.bootstrapTable('getData')[index];
        //console.log(dataRow);
        if (dataRow != null || dataRow != undefined) {
            loadPage(notePageId, {
                docassignid: dataRow.docassignid,
                pdocmatrix: dataRow.pdocmatrix,
                pic: dataRow.pic
            });
        }
    }

    object.loadFormEdit = function (obj, row, index, mode) {
        var dataRow = this.searchTable.bootstrapTable('getData')[index];
        currentDocAssign = dataRow['docassignid'];
        $("#modal-change-pic").modal('show');
    }

    object.loadUpload = function (obj, row, index, mode) {
        if (!variableExists('formPageId')) {
            return;
        }
        var dataRow = this.searchTable.bootstrapTable('getData')[index];
        if (dataRow != null || dataRow != undefined) {
            loadPage(uploadPageId, {
                docassignid: dataRow.docassignid,
                mode: mode
            });
        }
    }



    object.sendStatus = function (obj, row, index, mode) {

    }

}

/* registering refresh event of object */
pmtTable_prepare.refresh = function () {
    pmtTable_prepare.searchTable.bootstrapTable('refresh');
}
pmtTable_define.refresh = function () {
    pmtTable_define.searchTable.bootstrapTable('refresh');
}
pmtTable_design.refresh = function () {
    pmtTable_design.searchTable.bootstrapTable('refresh');
}
pmtTable_develop.refresh = function () {
    pmtTable_develop.searchTable.bootstrapTable('refresh');
}
pmtTable_deploy.refresh = function () {
    pmtTable_deploy.searchTable.bootstrapTable('refresh');
}
pmtTable_complete.refresh = function () {
    pmtTable_deploy.searchTable.bootstrapTable('refresh');
}
pmtTable_control.refresh = function () {
    pmtTable_control.searchTable.bootstrapTable('refresh');
}
pmtTable_other.refresh = function () {
    pmtTable_other.searchTable.bootstrapTable('refresh');
}
pmtTable_agreement.refresh = function () {
    pmtTable_agreement.searchTable.bootstrapTable('refresh');
}
pmtTable_detail.refresh = function () {
    pmtTable_detail.searchTable.bootstrapTable('refresh');
}
pmtTable_eacrevenue.refresh = function () {
    pmtTable_eacrevenue.searchTable.bootstrapTable('refresh');
    $(".revenueheader").parent().css("background-color", "#25AFEA");
}
pmtTable_eaccost.refresh = function () {
    pmtTable_eaccost.searchTable.bootstrapTable('refresh');
    $(".eacheader").parent().css("background-color", "#25AFEA");
}
pmtTable_fintrack.refresh = function () {
    pmtTable_fintrack.searchTable.bootstrapTable('refresh');

    $(".eacheader").parent().parent().addClass('row-header');
    manipulateFintrackTable();
    manipulateFintrackHeader();
    $(".eacheader").parent().css("background-color", "#25AFEA");
    $(".statusok").parent().css("background-color", "#CAF2B1");
    $(".statuswarning").parent().css("background-color", "#FFCE01");
    $(".statusdanger").parent().css("background-color", "#EBBEBC");
}

/* Compliance custom upload or view button and note button*/
pmtTable_prepare.renderActions = function (value, row, index) {
    //console.log(value);
    var objectReturn = create_div_action('pmtTable_prepare', row, index);
    return objectReturn.join('  ');
};
pmtTable_prepare.renderActionsNote = function (value, row, index) {
    var objectReturn = create_div_action_note('pmtTable_prepare', row, index);
    return objectReturn.join('  ');
};
pmtTable_prepare.renderActionsEdit = function (value, row, index) {
    var objectReturn = create_div_action_editPIC('pmtTable_prepare', row, index);
    return objectReturn.join('  ');
};
pmtTable_prepare.renderActionsDownload = function (value, row, index) {
    var objectReturn = create_div_action_download('pmtTable_prepare', row, index);
    return objectReturn.join('  ');
};
pmtTable_define.renderActions = function (value, row, index) {
    var objectReturn = create_div_action('pmtTable_define', row, index);
    return objectReturn.join('  ');
};
pmtTable_define.renderActionsNote = function (value, row, index) {
    var objectReturn = create_div_action_note('pmtTable_define', row, index);
    return objectReturn.join('  ');
};
pmtTable_define.renderActionsEdit = function (value, row, index) {
    var objectReturn = create_div_action_editPIC('pmtTable_define', row, index);
    return objectReturn.join('  ');
};
pmtTable_define.renderActionsDownload = function (value, row, index) {
    var objectReturn = create_div_action_download('pmtTable_define', row, index);
    return objectReturn.join('  ');
};
pmtTable_design.renderActions = function (value, row, index) {
    var objectReturn = create_div_action('pmtTable_design', row, index);
    return objectReturn.join('  ');
};
pmtTable_design.renderActionsNote = function (value, row, index) {
    var objectReturn = create_div_action_note('pmtTable_design', row, index);
    return objectReturn.join('  ');
};
pmtTable_design.renderActionsEdit = function (value, row, index) {
    var objectReturn = create_div_action_editPIC('pmtTable_design', row, index);
    return objectReturn.join('  ');
};
pmtTable_design.renderActionsDownload = function (value, row, index) {
    var objectReturn = create_div_action_download('pmtTable_design', row, index);
    return objectReturn.join('  ');
};
pmtTable_develop.renderActions = function (value, row, index) {
    var objectReturn = create_div_action('pmtTable_develop', row, index);
    return objectReturn.join('  ');
};
pmtTable_develop.renderActionsNote = function (value, row, index) {
    var objectReturn = create_div_action_note('pmtTable_develop', row, index);
    return objectReturn.join('  ');
};
pmtTable_develop.renderActionsEdit = function (value, row, index) {
    var objectReturn = create_div_action_editPIC('pmtTable_develop', row, index);
    return objectReturn.join('  ');
};
pmtTable_develop.renderActionsDownload = function (value, row, index) {
    var objectReturn = create_div_action_download('pmtTable_develop', row, index);
    return objectReturn.join('  ');
};
pmtTable_deploy.renderActions = function (value, row, index) {
    var objectReturn = create_div_action('pmtTable_deploy', row, index);
    return objectReturn.join('  ');
};
pmtTable_deploy.renderActionsNote = function (value, row, index) {
    var objectReturn = create_div_action_note('pmtTable_deploy', row, index);
    return objectReturn.join('  ');
};
pmtTable_deploy.renderActionsEdit = function (value, row, index) {
    var objectReturn = create_div_action_editPIC('pmtTable_deploy', row, index);
    return objectReturn.join('  ');
};
pmtTable_deploy.renderActionsDownload = function (value, row, index) {
    var objectReturn = create_div_action_download('pmtTable_deploy', row, index);
    return objectReturn.join('  ');
};
pmtTable_complete.renderActions = function (value, row, index) {
    var objectReturn = create_div_action('pmtTable_complete', row, index);
    return objectReturn.join('  ');
};
pmtTable_complete.renderActionsNote = function (value, row, index) {
    var objectReturn = create_div_action_note('pmtTable_complete', row, index);
    return objectReturn.join('  ');
};
pmtTable_complete.renderActionsEdit = function (value, row, index) {
    var objectReturn = create_div_action_editPIC('pmtTable_complete', row, index);
    return objectReturn.join('  ');
};
pmtTable_complete.renderActionsDownload = function (value, row, index) {
    var objectReturn = create_div_action_download('pmtTable_complete', row, index);
    return objectReturn.join('  ');
};
pmtTable_control.renderActions = function (value, row, index) {
    var objectReturn = create_div_action('pmtTable_other', row, index);
    return objectReturn.join('  ');
};
pmtTable_control.renderActionsNote = function (value, row, index) {
    var objectReturn = create_div_action_note('pmtTable_other', row, index);
    return objectReturn.join('  ');
};
pmtTable_control.renderActionsEdit = function (value, row, index) {
    var objectReturn = create_div_action_editPIC('pmtTable_other', row, index);
    return objectReturn.join('  ');
};
pmtTable_control.renderActionsDownload = function (value, row, index) {
    var objectReturn = create_div_action_download('pmtTable_other', row, index);
    return objectReturn.join('  ');
};
pmtTable_other.renderActions = function (value, row, index) {
    var objectReturn = create_div_action('pmtTable_other', row, index);
    return objectReturn.join('  ');
};
pmtTable_other.renderActionsNote = function (value, row, index) {
    var objectReturn = create_div_action_note('pmtTable_other', row, index);
    return objectReturn.join('  ');
};
pmtTable_other.renderActionsEdit = function (value, row, index) {
    var objectReturn = create_div_action_editPIC('pmtTable_other', row, index);
    return objectReturn.join('  ');
};
pmtTable_other.renderActionsDownload = function (value, row, index) {
    var objectReturn = create_div_action_download('pmtTable_other', row, index);
    return objectReturn.join('  ');
};

pmtTable_agreement.renderPic = function (value, row, index) {
    var objectReturn = create_div_action_PIC('pmtTable_agreement', row, index, value);
    return objectReturn.join('  ');
};

function create_div_action_download(table_name, row, index) {
    var objectReturn = ['<a href=' + row.url + '>' + row.deliverable];
    objectReturn.push('</a>');
    return objectReturn;
}
function create_div_action(table_name, row, index) {
    var objectReturn = ['<div class="btn-group-vertical class="margin-top:1rem;margin-bottom:1rem">'];
    objectReturn.push(create_string_button(table_name + '.loadUpload', row, index, 'upload', 'Upload'), );
    objectReturn.push(create_string_button(table_name + '.loadForm', row, index, 'view', 'View'), );
    objectReturn.push('</div>');
    return objectReturn;
}
function create_div_action_note(table_name, row, index) {
    var objectReturn = ['<div class="btn-group-vertical class="margin-top:1rem;margin-bottom:1rem">'];
    objectReturn.push(create_string_button(table_name + '.loadFormNote', row, index, 'view', 'Note'), );
    objectReturn.push('</div>');
    return objectReturn;
}
function create_div_action_editPIC(table_name, row, index) {
    var objectReturn = ['<div class="btn-group-vertical class="margin-top:1rem;margin-bottom:1rem">'];
    objectReturn.push(create_string_button(table_name + '.loadFormEdit', row, index, 'view', 'Edit'), );
    objectReturn.push('</div>');
    return objectReturn;
}
function create_div_action_PIC(table_name, row, index, value) {
    var objectReturn = ['<div class="btn-group-vertical class="margin-top:1rem;margin-bottom:1rem">'];
    objectReturn.push(create_string_select(value), );
    //objectReturn.push(create_string_button(table_name + '.loadFormEdit', row, index, 'view', 'Edit'), );
    objectReturn.push('</div>');
    return objectReturn;
}


function create_string_button(action, row, index, mode, text) {
    //console.log(row);
    return  `<button
            type="button"
            class="btn btn-primary" title="${text}"
            onclick="${action}(this,'${row}','${index}','${mode}')">
            ${text}
        </button>`
}

function create_string_select(value) {
    var selectString = "";

    if (pmtProjectMember != null && pmtProjectMember.length != 0) {
        selectString = '<select class="select_' + value + '" style="border-radius:4px">';
        for (i = 0; i < pmtProjectMember.length; i++) {
            selectString += '<option value="' + pmtProjectMemberEmail[i] + ':' + pmtProjectMember[i] + '" >' + pmtProjectMember[i] + '</option>';
        }
        selectString += '</select>';
    } else {
    }

    return selectString;
}

function append_change_all_pic() {
    if ($('#all_pic_selection').length != 0) {
        var selectString = "";

        if (pmtProjectMember != null && pmtProjectMember.length != 0) {
            for (i = 0; i < pmtProjectMember.length; i++) {
                selectString += '<option value="' + pmtProjectMemberEmail[i] + ':' + pmtProjectMember[i] + '" >' + pmtProjectMember[i] + '</option>';
            }

        } else {

        }
        $('#all_pic_selection').append(selectString);
    }
}

pmtTable_detail.renderActionsDownload = function (value, row, index) {
    var objectReturn = create_div_action_download_detail('pmtTable_detail', row, index);
    return objectReturn.join('  ');
};
function create_div_action_download_detail(table_name, row, index) {
    var objectReturn = ['<a href=' + row.url + ' target="_blank">' + row.filename];
    objectReturn.push('</a>');
    return objectReturn;
}

pmtTable_detail.renderActionsReupload = function (value, row, index) {
    var objectReturn = ['<div class="btn-group-vertical class="margin-top:1rem;margin-bottom:1rem">'];
    if (localStorage.projectUserRole == "PRJ") {

    } else {
        if (value == "") {
            objectReturn.push(create_string_button('pmtTable_detail.sendReview', row, index, 'review', 'Review'), );
        } else if (value == "Reviewed") {
            objectReturn.push(create_string_button('pmtTable_detail.sendAccept', row, index, 'review', 'Approve'), );
            objectReturn.push(create_string_button('pmtTable_detail.sendReject', row, index, 'review', 'Reject'), );
        } else {
            //objectReturn.push(create_string_button('pmtTable_detail.sendReview', row, index,'review','Review'), );
        }
    }

    objectReturn.push(create_string_button('pmtTable_detail.sendDelete', row, index, 'review', 'Delete'), );

    objectReturn.push('</div>');
    return objectReturn.join('  ');
};

pmtTable_detail.renderTaskActions = function (value, row, index) {
    var objectReturn = ['<div class="btn-group-vertical class="margin-top:1rem;margin-bottom:1rem">'];
    if (localStorage.projectUserRole == "PRJ") {
    } else {
        if (value == "") {
            objectReturn.push(create_string_button('pmtTable_detail.sendTaskReview', row, index, 'review', 'Review'), );
        } else if (value == "Reviewed") {
            objectReturn.push(create_string_button('pmtTable_detail.sendTaskAccept', row, index, 'review', 'Approve'), );
            objectReturn.push(create_string_button('pmtTable_detail.sendTaskReject', row, index, 'review', 'Reject'), );
        } else {
            //objectReturn.push(create_string_button('pmtTable_detail.sendTaskReview', row, index,'review','Review'), );
        }
    }
    objectReturn.push(create_string_button('pmtTable_detail.sendDelete', row, index, 'review', 'Delete'), );
    objectReturn.push('</div>');
    return objectReturn.join('  ');
};

pmtTable_detail.renderDate = function (value, row, index) {
    var date = new Date(value),
            yr = date.getFullYear(),
            month = date.getMonth() < 10 ? '0' + (Number(date.getMonth()) + 1) : (Number(date.getMonth()) + 1),
            day = date.getDate() < 10 ? '0' + date.getDate() : date.getDate(),
            hour = date.getHours(),
            minute = date.getMinutes(),
            newDate = day + '-' + month + '-' + yr + ' ' + hour + ':' + minute;
    objectReturn = newDate;
    return objectReturn;
}

pmtTable_detail.sendReview = function (obj, row, index, mode) {
    var dataRow = this.searchTable.bootstrapTable('getData')[index];
    updateStatusDoc(dataRow.docassignid, dataRow.docproid, '001');
    window.open(dataRow.url, '_blank');
}

pmtTable_detail.sendAccept = function (obj, row, index, mode) {
    var dataRow = this.searchTable.bootstrapTable('getData')[index];
    updateStatusDoc(dataRow.docassignid, dataRow.docproid, '002');
}

pmtTable_detail.sendReject = function (obj, row, index, mode) {
    var dataRow = this.searchTable.bootstrapTable('getData')[index];
    updateStatusDoc(dataRow.docassignid, dataRow.docproid, '003');
}

pmtTable_detail.sendDelete = function (obj, row, index, mode) {
    var dataRow = this.searchTable.bootstrapTable('getData')[index];
//    console.log(dataRow);
    deleteStatusDoc(dataRow.docassignid, dataRow.docproid);
}

pmtTable_detail.sendTaskReview = function (obj, row, index, mode) {
    var dataRow = this.searchTable.bootstrapTable('getData')[index];
    updateStatusDoc(dataRow.docassignid, dataRow.docproid, '001');
    showFurtherDetail();
    window.open(dataRow.url, '_blank');
}
pmtTable_detail.sendTaskAccept = function (obj, row, index, mode) {
    confirmTask4D('approve');
    showFurtherDetail();
}
pmtTable_detail.sendTaskReject = function (obj, row, index, mode) {
    $('#delegateModal').modal('show');
    showFurtherDetail();
}

var confirmTask4D = function (procedure) {
    if (procedure === 'reject' && $('#jdcfnd003-note4D').val().trim().length === 0) {
        var text = 'Please provide note for rejection.';
        var buttons = [{text: 'OK'}];
        showModalMessage(text, buttons);
        return;
    }

    var text = `Do you want to ${procedure} the task?`;
    var buttons = [
        {
            text: 'Yes',
            action: performTask4D.bind(null, procedure),
        }, {text: 'No'}
    ];
    $('#delegateModal').modal('hide');
    hideFurtherDetail();
    showModalMessage(text, buttons);
}

var performTask4D = function (procedure) {
    $("#loader-submit").show();
    var url = `/svc-jdc-project-management/rest/jdc/pmg000/approve-task`;
    if (!selectedTask) {
        console.log('Selected task is not defined');
    }
    if (!selectedTask.idTask) {
        console.log('Cannot find idTask in this object: ', selectedTask);
    }
    var statusCode;
    if (procedure == "reject") {
        statusCode = "005";
    } else if (procedure == "approve") {
        statusCode = "004";
    } else if (procedure == "escalate") {
        statusCode = "011"
    }
    var payload = {
        projectNumber: selectedTask.desc1,
        idTask: selectedTask.idTask,
        status: statusCode,
        note: $('#jdcfnd003-note4D').val(),
        progress: $('#jdcfnd003-progress').val(),
        result: $('#jdcfnd003-result').val(),
        description: $('#jdcfnd003-description').val(),
        achievement: $('#jdcfnd003-achievement').val()
    };
    console.log(payload);
    postRequest(url, payload, function () {
        page.loadTaskList();
        hideTaskDetail();
    });
};

function updateStatusDoc(docassignid, docproid, status) {
    var url = '/svc-jdc-project-management-tools/rest/jdc/pmt001/update-statusdoc';
    var payload = {
        "docassignid": docassignid,
        "docproid": docproid,
        "status": status
    };
    var text2 = `Updating status`;
    showModalMessage(text2);

    postRequest(url, payload, function (result) {
        //console.log(result);
        if (result['status'] == 1) {
            var text = `Status has been updated`;
            if (status == '001') {
                text = `Download the document for review it`;
            } else if (status == '002') {
                text = `Document has been mark as approved`;
            } else if (status == '003') {
                text = `Document has been mark as rejected`;
            }
            var buttons = [{
                    text: "Ok"
                }
            ];
            showModalMessage(text, buttons);
            pmtTable_detail.refresh();
        }
    });
}

function deleteStatusDoc(docassignid, docproid) {
    var url = '/svc-jdc-project-management-tools/rest/jdc/pmt001/delete-doc';
    var payload = {
        "docassignid": docassignid,
        "docproid": docproid
    };

    var text2 = `Updating status`;
    showModalMessage(text2);

    postRequest(url, payload, function (result) {
        //console.log(result);
        if (result['status'] == 1) {

            var text = `Document has been deleted`;

            var buttons = [{
                    text: "Ok"
                }
            ];
            showModalMessage(text, buttons);
            pmtTable_detail.refresh();
        }
    });
}


function applyCommonParam(current_param, source_param) {
    dest_param = current_param
    if (source_param) {
        if (source_param.limit) {
            dest_param.sort = source_param.sort;
            dest_param.order = source_param.order;
            dest_param.limit = source_param.limit;
            dest_param.offset = source_param.offset;
        }
    }
    return dest_param
}
pmtTable_prepare.searchParameters = function (params) {
    var payload = {
        search: {
            "=pnumber": localStorage.projectNumber,
            "=projectUserRole": localStorage.projectUserRole,
            "=phase": '001'
        }
    };
    payload = applyCommonParam(payload, params.data);
    return payload;
}
pmtTable_define.searchParameters = function (params) {
    var payload = {
        search: {
            "=pnumber": localStorage.projectNumber,
            "=projectUserRole": localStorage.projectUserRole,
            "=phase": '002'
        }
    };
    payload = applyCommonParam(payload, params.data);
    return payload;
}
pmtTable_design.searchParameters = function (params) {
    var payload = {
        search: {
            "=pnumber": localStorage.projectNumber,
            "=projectUserRole": localStorage.projectUserRole,
            "=phase": '003'
        }
    };
    payload = applyCommonParam(payload, params.data);
    return payload;
}
pmtTable_develop.searchParameters = function (params) {
    var payload = {
        search: {
            "=pnumber": localStorage.projectNumber,
            "=projectUserRole": localStorage.projectUserRole,
            "=phase": '004'
        }
    };
    payload = applyCommonParam(payload, params.data);
    return payload;
}

pmtTable_deploy.searchParameters = function (params) {
    var payload = {
        search: {
            "=pnumber": localStorage.projectNumber,
            "=projectUserRole": localStorage.projectUserRole,
            "=phase": '005'
        }
    };
    payload = applyCommonParam(payload, params.data);
    return payload;
}

pmtTable_complete.searchParameters = function (params) {
    var payload = {
        search: {
            "=pnumber": localStorage.projectNumber,
            "=projectUserRole": localStorage.projectUserRole,
            "=phase": '006'
        }
    };
    payload = applyCommonParam(payload, params.data);
    return payload;
}
pmtTable_control.searchParameters = function (params) {
    var payload = {
        search: {
            "=pnumber": localStorage.projectNumber,
            "=projectUserRole": localStorage.projectUserRole,
            "=phase": '008'
        }
    };
    payload = applyCommonParam(payload, params.data);
    return payload;
}
pmtTable_other.searchParameters = function (params) {
    var payload = {
        search: {
            "=pnumber": localStorage.projectNumber,
            "=projectUserRole": localStorage.projectUserRole,
            "=phase": '007'
        }
    };
    payload = applyCommonParam(payload, params.data);
    return payload;
}
pmtTable_agreement.searchParameters = function (params) {
    var payload = {
        search: {
            "=pnumber": localStorage.projectNumber,
            "=method": $('#method_selection').val(),
            "=methodtype": $('#type_selection').val()
        }
    };
    payload = applyCommonParam(payload, params.data);
    return payload;
}
pmtTable_detail.searchParameters = function (params) {
    var payload = {
        search: {
            "=docassignid": pageParameters.docassignid
        }
    };
    payload = applyCommonParam(payload, params.data);
    return payload;
}
pmtTable_agreement.searchParameters2 = function (params) {
    var payload = {
        "projectNumber": localStorage.projectNumber,
        "projectType": '001'

    };
    return payload;
}

pmtTable_eacrevenue.searchParameters = function (params) {
    var payload = {
        search: {
            "=pnumber": localStorage.projectNumber
        }
    };
    payload = applyCommonParam(payload, params.data);
    return payload;
}

pmtTable_eaccost.searchParameters = function (params) {
    var payload = {
        search: {
            "=pnumber": localStorage.projectNumber,
            "=month": parseInt($('#monthfilter').val()),
            "=year": parseInt($('#yearfilter').val()),
            "=subject": $('#subjectfilter').val()
        }
    };
    payload = applyCommonParam(payload, params.data);
    return payload;
}

pmtTable_fintrack.searchParameters = function (params) {
    var payload = {
        search: {
            "=pnumber": localStorage.projectNumber,
        }
    };
    payload = applyCommonParam(payload, params.data);
    return payload;
}

pmtTable_fintrack_actual.searchParameters = function (params) {
    var payload = {
        search: {
            "=pnumber": localStorage.projectNumber,
            "%vwbsno": window.wbs,
            "%gl": window.gl
        }
    };
    payload = applyCommonParam(payload, params.data);
    return payload;
}

pmtTable_fintrack_budget.searchParameters = function (params) {
    var payload = {
        search: {
            "=pnumber": localStorage.projectNumber,
            "%vwbsno": window.wbs,
            "%gl": window.gl
        }
    };
    payload = applyCommonParam(payload, params.data);
    return payload;
}

pmtTable_fintrack_commit.searchParameters = function (params) {
    var payload = {
        search: {
            "=pnumber": localStorage.projectNumber,
            "%vwbsno": window.wbs,
            "%gl": window.gl
        }
    };
    payload = applyCommonParam(payload, params.data);
    return payload;
}

pmtTable_prepare.ajax = function (params) {
    if (!variableExists('searchTableUrl')) {
        return;
    }
    var payload = pmtTable_prepare.searchParameters(params);
    postRequest(window.searchTableUrl, payload, function (result) {
        params.success(result);
    });
};

pmtTable_define.ajax = function (params) {
    if (!variableExists('searchTableUrl')) {
        return;
    }
    var payload = pmtTable_define.searchParameters(params);
    postRequest(window.searchTableUrl, payload, function (result) {
        params.success(result);
    });
};

pmtTable_design.ajax = function (params) {
    if (!variableExists('searchTableUrl')) {
        return;
    }
    var payload = pmtTable_design.searchParameters(params);
    postRequest(window.searchTableUrl, payload, function (result) {
        params.success(result);
    });
};

pmtTable_develop.ajax = function (params) {
    if (!variableExists('searchTableUrl')) {
        return;
    }
    var payload = pmtTable_develop.searchParameters(params);
    postRequest(window.searchTableUrl, payload, function (result) {
        params.success(result);
    });
};

pmtTable_deploy.ajax = function (params) {
    if (!variableExists('searchTableUrl')) {
        return;
    }
    var payload = pmtTable_deploy.searchParameters(params);
    postRequest(window.searchTableUrl, payload, function (result) {
        params.success(result);
    });
};

pmtTable_complete.ajax = function (params) {
    if (!variableExists('searchTableUrl')) {
        return;
    }
    var payload = pmtTable_complete.searchParameters(params);
    postRequest(window.searchTableUrl, payload, function (result) {
        params.success(result);
    });
};

pmtTable_control.ajax = function (params) {
    if (!variableExists('searchTableUrl')) {
        return;
    }
    var payload = pmtTable_control.searchParameters(params);
    postRequest(window.searchTableUrl, payload, function (result) {
        params.success(result);
    });
};

pmtTable_other.ajax = function (params) {
    if (!variableExists('searchTableUrl')) {
        return;
    }
    var payload = pmtTable_other.searchParameters(params);
    postRequest(window.searchTableUrl, payload, function (result) {
        params.success(result);
    });
};
pmtTable_agreement.ajax = function (params) {
    if (!variableExists('searchTableUrl')) {
        return;
    }
    var payload = pmtTable_agreement.searchParameters(params);
    var payload2 = pmtTable_agreement.searchParameters2(params);
    postRequest(window.searchTableUrl2, payload2, function (result) {
        //console.log(result.data);
        if (result.data != undefined && result.data != null) {
            for (i = 0; i < result['data'].length; i++) {
                pmtProjectMember.push(result['data'][i]['fullname']);
            }
            for (i = 0; i < result['data'].length; i++) {
                pmtProjectMemberEmail.push(result['data'][i]['email']);
            }
        }
        append_change_all_pic();
    });
    postRequest(window.searchTableUrl, payload, function (result) {
        params.success(result);
        console.log(result.data);
        checkAgreement();

        result.data.forEach(data => {
            if (localStorage.role == 'ADM') {
                if (data["isDoc"] == "true") {
                    $("tr[data-uniqueid='" + data['pdocmatrix'] + "']").find('input:checkbox:first').attr('disabled', true);
                }
            } else {
                $("tr[data-uniqueid='" + data['pdocmatrix'] + "']").find('input:checkbox:first').attr('disabled', true);
            }
        });
//        .forEach(selectedDatum => {
//            console.log(selectedDatum['pdocmatrix']);
//            console.log(removedindex);
//            if(!removedindex.includes(selectedDatum['pdocmatrix'])){
//                selectedDataFinal.push(selectedDatum);
//            }
//        });
    });
};
pmtTable_agreement.performProcedure = function (procedureName, selectedDoc) {
    if (!variableExists('procedureUrls')) {
        return;
    }
    var url = procedureUrls[procedureName];
    var payload = selectedDoc.map(function (selectedDoc) {
        return {
            pdocmatrix: selectedDoc.pdocmatrix,
            pnumber: localStorage.projectNumber,
            pic: selectedDoc.email,
            checked: selectedDoc.checked
        }
    });

    postRequest(url, payload, function (result) {
        //console.log(result['message']);
        if (result['message']['message'] == 'ERROR') {
            showModalMessage(result['message']['error']);
        }
        pmtTable_agreement.refresh;
    });
};
pmtTable_detail.ajax = function (params) {
    if (!variableExists('searchTableUrl')) {
        return;
    }
    var payload = pmtTable_detail.searchParameters(params);
    //console.log(payload);
    postRequest(window.searchTableUrl, payload, function (result) {
        params.success(result);
        //console.log(params);
    });

};

pmtTable_eacrevenue.ajax = function (params) {
    if (!variableExists('searchTableRevenueUrl')) {
        return;
    }
    var payload = pmtTable_eacrevenue.searchParameters(params);
    postRequest(window.searchTableRevenueUrl, payload, function (result) {
        params.success(result);
        console.log(result);
    });
};

pmtTable_eaccost.ajax = function (params) {
    if (!variableExists('searchTableUrl')) {
        return;
    }
    var payload = pmtTable_eaccost.searchParameters(params);
    postRequest(window.searchTableUrl, payload, function (result) {
        params.success(result);
    });
};

pmtTable_fintrack.ajax = function (params) {
    if (!variableExists('searchTableUrl')) {
        return;
    }
    var payload = pmtTable_fintrack.searchParameters(params);
    postRequest(window.searchTableUrl, payload, function (result) {
        params.success(result);
        //manipulateFintrackTable();
    });
};

pmtTable_fintrack_actual.ajax = function (params) {
    if (!variableExists('searchTableUrl')) {
        return;
    }
    var payload = pmtTable_fintrack_actual.searchParameters(params);
    postRequest(window.searchTableUrl, payload, function (result) {
        params.success(result);
    });
};

pmtTable_fintrack_budget.ajax = function (params) {
    if (!variableExists('searchTableUrl')) {
        return;
    }
    var payload = pmtTable_fintrack_budget.searchParameters(params);
    postRequest(window.searchTableUrl, payload, function (result) {
        params.success(result);
    });
};

pmtTable_fintrack_commit.ajax = function (params) {
    if (!variableExists('searchTableUrl')) {
        return;
    }
    var payload = pmtTable_fintrack_commit.searchParameters(params);
    postRequest(window.searchTableUrl, payload, function (result) {
        params.success(result);
    });
};

pmtTable_other.initialize = function () {
    this.searchTable = $(`#${otherTableId}`);
    this.searchTable.bootstrapTable();
};

function checkAgreement() {
    $("input[type=checkbox]")  // for all checkboxes
            .each(function () {  // first pass, create name mapping
                this.closest('tr').classList.add('pdocchecked');
                var classlist = this.closest('tr').getAttribute('class');
                if (this.checked && classlist.split(' ')[0] == 'selected') {
                    this.closest('tr').classList.add('checked');
                    isAlreadySubmitted = true;
                }
            });
    if (!isAlreadySubmitted) {
        if (localStorage.role == 'ADM') {
            $("input[type=checkbox]")  // for all checkboxes
                    .each(function () {
                        $(this).attr('checked', true);
                    });
        }
    }
}



$('body').on('change', '.checkbox1', function () {
    // do something
    alert('dodol');
});

function handleclick() {
    console.log('check');
}

function getPercentage() {
    initiateValuePercentage(0, 0, 0, 0, 0, 0, 0);
    var searchaTableUrl = '/svc-jdc-project-management-tools/rest/jdc/pmt001/get-percentage';
    var payload = {
        "pnumber": localStorage.projectNumber
    };
    postRequest(searchaTableUrl, payload, function (result) {

        var val_pmt4dprepare = 0;
        var val_pmt4ddefine = 0;
        var val_pmt4ddesign = 0;
        var val_pmt4ddevelop = 0;
        var val_pmt4ddeploy = 0;
        var val_pmt4dcomplete = 0;
        var val_pmt4dtotal = 0;
        var val_pmt4dphase = '001';
        //console.log(result);

        if (result['message']['001'] != undefined && result['message']['001'] != null) {
            val_pmt4dprepare = result['message']['001'];
        }
        if (result['message']['002'] != undefined && result['message']['002'] != null) {
            val_pmt4ddefine = result['message']['002'];
        }
        if (result['message']['003'] != undefined && result['message']['003'] != null) {
            val_pmt4ddesign = result['message']['003'];
        }
        if (result['message']['004'] != undefined && result['message']['004'] != null) {
            val_pmt4ddevelop = result['message']['004'];
        }
        if (result['message']['005'] != undefined && result['message']['005'] != null) {
            val_pmt4ddeploy = result['message']['005'];
        }
        if (result['message']['006'] != undefined && result['message']['006'] != null) {
            val_pmt4dcomplete = result['message']['006'];
        }

        if (result['data'][0]['total'] != undefined && result['data'][0]['total'] != null && result['data'][0]['total'] != 'NaN') {
            val_pmt4dtotal = result['data'][0]['total'];
        }
        if (result['data'][0]['phase'] != undefined && result['data'][0]['phase'] != null) {
            val_pmt4dphase = result['data'][0]['phase'];
        }

        initiateValuePercentage(val_pmt4dprepare, val_pmt4ddefine, val_pmt4ddesign, val_pmt4ddevelop, val_pmt4ddeploy, val_pmt4dcomplete, val_pmt4dtotal, val_pmt4dphase);
//        changing_bar_color('pmt4dprepare_bar',100);
//        changing_bar_color('pmt4ddefine_bar',75);
//        changing_bar_color('pmt4ddesign_bar',49);
//        changing_bar_color('pmt4ddevelop_bar',15);
//        changing_bar_color('pmt4ddeploy_bar',result['message']['005']);
//        changing_bar_color('pmt4dcomplete_bar',result['message']['006']);
    });
}

function initiateValuePercentage(val_pmt4dprepare, val_pmt4ddefine, val_pmt4ddesign, val_pmt4ddevelop, val_pmt4ddeploy, val_pmt4dcomplete, val_overallval, val_currentphase) {
    //console.log(val_pmt4dprepare);
    if (val_pmt4dprepare > 0) {
        val_pmt4dprepare = val_pmt4dprepare.toFixed(2);
    }
    if (val_pmt4ddefine > 0) {
        val_pmt4ddefine = val_pmt4ddefine.toFixed(2);
    }
    if (val_pmt4ddesign > 0) {
        val_pmt4ddesign = val_pmt4ddesign.toFixed(2);
    }
    if (val_pmt4ddevelop > 0) {
        val_pmt4ddevelop = val_pmt4ddevelop.toFixed(2);
    }
    if (val_pmt4ddeploy > 0) {
        val_pmt4ddeploy = val_pmt4ddeploy.toFixed(2);
    }
    if (val_pmt4dcomplete > 0) {
        val_pmt4dcomplete = val_pmt4dcomplete.toFixed(2);
    }
    //    if (val_overallval > 0) { val_overallval = val_overallval.toFixed(2); }
    if (val_overallval > 0) {
        val_overallval = parseFloat(val_overallval).toFixed(2);
    }


    $('#pmt4dprepare_val').html(val_pmt4dprepare);
    $('#pmt4ddefine_val').html(val_pmt4ddefine);
    $('#pmt4ddesign_val').html(val_pmt4ddesign);
    $('#pmt4ddevelop_val').html(val_pmt4ddevelop);
    $('#pmt4ddeploy_val').html(val_pmt4ddeploy);
    $('#pmt4dcomplete_val').html(val_pmt4dcomplete);
    changing_bar_color('pmt4dprepare_bar', val_pmt4dprepare);
    changing_bar_color('pmt4ddefine_bar', val_pmt4ddefine);
    changing_bar_color('pmt4ddesign_bar', val_pmt4ddesign);
    changing_bar_color('pmt4ddevelop_bar', val_pmt4ddevelop);
    changing_bar_color('pmt4ddeploy_bar', val_pmt4ddeploy);
    changing_bar_color('pmt4dcomplete_bar', val_pmt4dcomplete);

    var currentvalue = 0;
    var currentphase = "";
    var overallphase = "";
    if (val_currentphase == '001') {
        currentvalue = val_pmt4dprepare;
        currentphase = 'Prepare';
        overallphase = 'Prepare';
    } else if (val_currentphase == '002') {
        currentvalue = val_pmt4ddefine;
        currentphase = 'Define';
        overallphase = 'Prepare - Define';
    } else if (val_currentphase == '003') {
        currentvalue = val_pmt4ddesign;
        currentphase = 'Design';
        overallphase = 'Prepare - Design';
    } else if (val_currentphase == '004') {
        currentvalue = val_pmt4ddevelop;
        currentphase = 'Develop';
        overallphase = 'Prepare - Develop';
    } else if (val_currentphase == '005') {
        currentvalue = val_pmt4ddeploy;
        currentphase = 'Deploy';
        overallphase = 'Prepare - Deploy';
    } else if (val_currentphase == '006') {
        currentvalue = val_pmt4dcomplete;
        currentphase = 'Complete';
        overallphase = 'Prepare - Complete';
    }

    var overallvalue = val_overallval;
    //console.log(val_pmt4dprepare);

    $('#current_state').html(currentphase);
    $('#overall_state').html(overallphase);
    changing_state_color('overall_val', 'overall_bar', overallvalue);
    changing_state_color('current_val', 'current_bar', currentvalue);
}

function refreshajaxcall() {
    pmtTable_agreement.refresh();
}

function refreshajaxcalleac() {
    pmtTable_eaccost.refresh();
}

function refreshajaxcallft() {
    pmtTable_fintrack.refresh();
}

function changing_bar_color(name, value)
{
    var lastClass = $('#' + name).attr('class').split(' ').pop();
    $('#' + name).removeClass(lastClass);

    if (value > 75) {
        $('#' + name).addClass('progress-bar-complete');
        $('#' + name).css('width', value + '%');
    } else if (value > 51 && value <= 75) {
        $('#' + name).addClass('progress-bar-success');
        $('#' + name).css('width', value + '%');
    } else if (value > 26 && value <= 50) {
        $('#' + name).addClass('progress-bar-warning');
        $('#' + name).css('width', value + '%');
    } else {
        $('#' + name).css('width', value + '%');
        $('#' + name).addClass('progress-bar-danger');
    }
}

function changing_state_color(numbername, barname, value)
{
    $('#' + numbername).html(value + '%');
    var lastClass = $('#' + numbername).attr('class').split(' ').pop();
    $('#' + numbername).removeClass(lastClass);
    var lastClass2 = $('#' + barname).attr('class').split(' ').pop();
    $('#' + barname).removeClass(lastClass2);

    if (value > 75) {
        $('#' + numbername).addClass('text-blue');
        $('#' + barname).addClass('progress-bar-complete');
        $('#' + barname).css('width', value + '%');
    } else if (value > 51 && value <= 75) {
        $('#' + numbername).addClass('text-green');
        $('#' + barname).addClass('progress-bar-success');
        $('#' + barname).css('width', value + '%');
    } else if (value > 26 && value <= 50) {
        $('#' + numbername).addClass('text-yellow');
        $('#' + barname).addClass('progress-bar-warning');
        $('#' + barname).css('width', value + '%');
    } else {
        $('#' + numbername).addClass('text-red');
        $('#' + barname).addClass('progress-bar-danger');
        $('#' + barname).css('width', value + '%');
    }
}

function getProjectMember() {

    var searchTableUrlMember = '/svc-jdc-project-management/rest/jdc/pmg000/get-data-member-organization'
    var payloadMember = {
        "projectNumber": localStorage.projectNumber,
        "projectType": '001'

    };
    postRequest(searchTableUrlMember, payloadMember, function (result) {
        if (result.data != undefined && result.data != null) {
            for (i = 0; i < result['data'].length; i++) {
                pmtProjectMember.push(result['data'][i]['fullname']);
            }
            for (i = 0; i < result['data'].length; i++) {
                pmtProjectMemberEmail.push(result['data'][i]['email']);
            }
        }

        if (pmtProjectMember != null && pmtProjectMember.length != 0) {
            selectString = '<select id="select_change_pic" class="form-control select_member" style="width: 60%;margin: 0 auto;">';
            for (i = 0; i < pmtProjectMember.length; i++) {
                selectString += '<option value="' + pmtProjectMemberEmail[i] + ':' + pmtProjectMember[i] + '" >' + pmtProjectMember[i] + '</option>';
            }
            selectString += '</select>';
        } else {
        }
        $('#modal-change-pic-holder').append(selectString);


    });
}

function changePic() {
    var urlchange = "/svc-jdc-project-management-tools/rest/jdc/pmt001/update-docassign";

    tempselect = $("#select_change_pic").val();
    var emailPic = tempselect.split(':')[0];
    console.log(emailPic);
    console.log(currentDocAssign);
    console.log(window.currentTab);

    var payloadchange = {
        "docassignid": currentDocAssign,
        "pic": emailPic,
    };
    postRequest(urlchange, payloadchange, function (result) {
        console.log(result);
        if (result['status'] == 1) {
            var text = `PIC has been changed`;
            var buttons = [{
                    text: "Ok"
                }
            ];
            showModalMessage(text, buttons);
            refreshEditPIC();


        } else {
            var text = `Error change PIC`;
            var buttons = [{
                    text: "Ok"
                }
            ];
            showModalMessage(text, buttons);
        }
    });
}

function refreshEditPIC() {
    var tabName = window.currentTab;
    console.log(tabName);
    if (tabName == 'prepare4d') {
        pmtTable_prepare.searchTable.bootstrapTable('refresh');
    } else if (tabName == 'define4d') {
        pmtTable_define.searchTable.bootstrapTable('refresh');
    } else if (tabName == 'design4d') {
        pmtTable_design.searchTable.bootstrapTable('refresh');
    } else if (tabName == 'develop4d') {
        pmtTable_develop.searchTable.bootstrapTable('refresh');
    } else if (tabName == 'deploy4d') {
        pmtTable_deploy.searchTable.bootstrapTable('refresh');
    } else if (tabName == 'complete4d') {
        pmtTable_complete.searchTable.bootstrapTable('refresh');
    } else if (tabName == 'control4d') {
        pmtTable_control.searchTable.bootstrapTable('refresh');
    } else if (tabName == 'other4d') {
        pmtTable_other.searchTable.bootstrapTable('refresh');
    }
}

function manipulateFintrackTable() {
    var prevrow = "";
    var indexclass = 1;
    var count = 1;
    var isfirst = true;
    $('#jdcpmt0007-fintrack tr:nth-child(1)').each(function () {
        $(this).find("th:nth-child(1)").css("text-align", "left");
        $(this).find("th:nth-child(1)").attr('colspan', 3);
        $(this).find("th:nth-child(2)").addClass('rowhidden');
        $(this).find("th:nth-child(3)").addClass('rowhidden');
    });
    $('#jdcpmt0007-fintrack tr:not(.row-header)').each(function () {
        if ($(this).find("td:nth-child(1)").html() !== undefined) {
            if ($(this).find("td:nth-child(1)").html() != prevrow) {
                $('.rowspans3_' + indexclass).attr('colspan', 3);
                if (isfirst) {
                    isfirst = false;
                    prevrow = $(this).find("td:nth-child(1)").html();
                    $(this).find("td:nth-child(1)").addClass('rowhidden');
                    $(this).find("td:nth-child(2)").addClass('rowhidden');
                    //$(this).find("td:nth-child(2)").addClass('rowspans2_' + indexclass);
                    $(this).find("td:nth-child(3)").addClass('rowspans3_' + indexclass);
                    $(this).find("td:nth-child(3)").addClass('left-align');
                } else {
                    if (count > 1) {
                        //$('.rowspans_' + indexclass).attr('rowspan', count);

                        $('.rowspans2_' + indexclass).attr('rowspan', count);
                        $('.rowspans3_' + indexclass).attr('rowspan', count);
                    }
                    indexclass++;
                    prevrow = $(this).find("td:nth-child(1)").html();
                    $(this).find("td:nth-child(1)").addClass('rowhidden');
                    $(this).find("td:nth-child(2)").addClass('rowhidden');
                    //$(this).find("td:nth-child(2)").addClass('rowspans2_' + indexclass);
                    $(this).find("td:nth-child(3)").addClass('rowspans3_' + indexclass);
                    $(this).find("td:nth-child(3)").addClass('left-align');
                    count = 1;
                }
            } else {
                $(this).find("td:nth-child(1)").addClass('rowhidden');
                $(this).find("td:nth-child(2)").addClass('rowhidden');
                $(this).find("td:nth-child(3)").addClass('rowhidden');
                count++;
            }
        }



        console.log($(this).attr("class"));
    });

    $('.rowspans3_' + indexclass).attr('colspan', 3);
    $('.rowspans2_' + indexclass).attr('rowspan', count);
    $('.rowspans3_' + indexclass).attr('rowspan', count);

}

function manipulateFintrackHeader() {
    $('#jdcpmt0007-fintrack tr.row-header').each(function () {
        $(this).find("td:nth-child(1)").css("cssText", "text-align: left; padding-left: 10px!important;");

        $(this).find("td:nth-child(1)").attr('colspan', 5);
        $(this).find("td:nth-child(2)").addClass('rowhidden');
        $(this).find("td:nth-child(3)").addClass('rowhidden');
        $(this).find("td:nth-child(4)").addClass('rowhidden');
        $(this).find("td:nth-child(5)").addClass('rowhidden');
    });

}

function manipulateRevenueHeader() {
    $('#jdcpmt0006-eacrevenue tr.row-header').each(function () {
        $(this).find("td:nth-child(1)").css("cssText", "text-align: left;");

        $(this).find("td:nth-child(1)").attr('colspan', 4);
        $(this).find("td:nth-child(2)").addClass('rowhidden');
        $(this).find("td:nth-child(3)").addClass('rowhidden');
        $(this).find("td:nth-child(4)").addClass('rowhidden');
    });

}

function fTlink(projectNumber, wbs, gl, mode) {
    console.log('kupu-kupu');
    if (mode == 'actual') {
        loadPage('JDCPMT0007-actual', {
            projectNumber: projectNumber,
            wbs: wbs,
            gl: gl
        });
    } else if (mode == 'budget') {
        loadPage('JDCPMT0007-budget', {
            projectNumber: projectNumber,
            wbs: wbs,
            gl: gl
        });
    } else {
        loadPage('JDCPMT0007-commit', {
            projectNumber: projectNumber,
            wbs: wbs,
            gl: gl
        });
    }
}