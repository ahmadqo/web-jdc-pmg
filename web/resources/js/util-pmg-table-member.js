pmgTableMember = {
    searchTable: {}
};

pmgTableMember.initialize = function () {
    var invalidSymbols = [
        "-",
        "+",
        "e",
    ];
    if (!variablesExist('searchTableIdMember')) {
        return;
    }
    this.searchTable = $(`#${searchTableIdMember}`);
    this.searchTable.bootstrapTable();

    $('thead select.member').on('change', this.refresh);
    $('this input.member').on('keypress', function (event) {
        if (event.which === 13) {
            event.preventDefault();
            this.refresh();
        }
    }.bind(this));

    $('thead .numbermember').on('keydown', function (event) {
        if (invalidSymbols.includes(event.key)) {
            event.preventDefault();
        }
    });
}

pmgTableMember.refresh = function () {
    pmgTableMember.searchTable.bootstrapTable('refresh');
}

pmgTableMember.ajax = function (params) {
    if (!variablesExist(['searchTableUrlMember', 'searchParametersMember'])) {
        return;
    }
    var payload = searchParametersMember(params);
    postRequest(window.searchTableUrlMember, payload, function (result) {
        params.success(result);
    });
}

pmgTableMember.deleteData = function (dataToDelete) {
    if (!variablesExist(['entityNameMember', 'pmgDeleteUrlContract'])) {
        return;
    }
    var processResult = function (data) {
        if (data !== null) {
            if (data.status == "1") {
                setMessage(`${entityNameMember} has been deleted successfully!`, null, `member`);
                this.searchTable.bootstrapTable('refresh');
            } else {
                setMessage(`Error in deleting the ${entityNameMember} !`, 'danger', `member`);
            }
        }
    };
    postRequest(pmgDeleteUrlContract, dataToDelete, processResult.bind(this));
}

pmgTableMember.deletePopUp = function (obj, row, index) {
    if (!variablesExist(['entityNameMember', 'pmgDeleteUrlContract'])) {
        return;
    }
    var dataRow = this.searchTable.bootstrapTable('getData')[index];

    var text = `Are you sure you want to delete ${entityNameMember}`;
    var buttons = [
        {
            text: "Yes",
            action: this.deleteData.bind(this, dataRow)
        }, {
            text: "No"
        }
    ];
    showModalMessage(text, buttons)
}

pmgTableMember.loadForm = function (obj, row, index, mode) {
    if (!variablesExist(['pmgFormPageMember', 'pmgFormParametersMember'])) {
        return;
    }
    let dataRow = this.searchTable.bootstrapTable('getData')[index];
    loadPage(pmgFormPageMember, pmgFormParametersMember(dataRow, mode));
}

pmgTableMember.renderUpdateActions = function (value, row, index) {
    var objectReturn = ['<div class="btn-group-vertical class="margin-top:1rem;margin-bottom:1rem">'];
    objectReturn.push(
            `<button 
            class="btn btn-primary" 
            onclick="pmgTableMember.loadForm(this,'${row}','${index}','view')" 
            title="View"
        >
            View
        </button>`
            );
    objectReturn.push(
            `<button 
            class="btn btn-primary"
            onclick="pmgTableMember.loadForm(this,'${row}','${index}','edit')" 
            title="Edit"
        >
            Edit
        </button>`,
            );
    if (!row.status.toLowerCase() == "draft" && row.isCreator == true) {
        objectReturn.push(
                `<button 
                class="btn btn-primary" 
                onclick="pmgTableMember.deletePopUp(this,'${row}','${index}')" 
                title="Delete" 
            >
                Delete
            </button>`,
                );
    }
    objectReturn.push('</div>');
    return objectReturn.join(' ');
}

//pmgTableMember.renderActionAdmin = function (value, row, index) {
//    var objectReturn = ['<div class="btn-group-vertical class="margin-top:1rem;margin-bottom:1rem">'];
//    objectReturn.push(
//            `<button type="button" id="buttonDelegate"
//                class="btn btn-primary" data-toggle="modal" 
//                onclick="myFunction()""
//                data-target="#delegateModal" title="Delegate" >
//               
//        </button>`,
//            );
//    objectReturn.push('</div>');
//    return objectReturn.join(' ');
//}
