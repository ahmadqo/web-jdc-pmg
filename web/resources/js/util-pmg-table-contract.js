pmgTableContract = {
    searchTable: {}
};

pmgTableContract.initialize = function () {
    var invalidSymbols = [
        "-",
        "+",
        "e",
    ];
    if (!variableExists('searchTableIdContract')) {
        return;
    }
    this.searchTable = $(`#${searchTableIdContract}`);
    this.searchTable.bootstrapTable();

    $('thead select.contract').on('change', this.refresh);
    $('thead input.contract').on('keypress', function (event) {
        if (event.which === 13) {
            event.preventDefault();
            this.refresh();
        }
    }.bind(this));

    $('thead .numbercontract').on('keydown', function (event) {
        if (invalidSymbols.includes(event.key)) {
            event.preventDefault();
        }
    });
}

pmgTableContract.refresh = function () {
    pmgTableContract.searchTable.bootstrapTable('refresh');
    if (["PRG", "DP"].indexOf(localStorage.projectUserRole) > -1) {
        $('table .actionTable').hide();
    }
}

pmgTableContract.ajax = function (params) {
    if (!variablesExist(['searchTableUrlContract', 'searchParametersContract'])) {
        return;
    }
    var payload = searchParametersContract(params);
    postRequest(window.searchTableUrlContract, payload, function (result) {
        params.success(result);
    });
}

pmgTableContract.deleteData = function (dataToDelete) {
    if (!variablesExist(['entityNameContract', 'pmgDeleteUrlContract'])) {
        return;
    }

    var processResult = function (data) {
        if (data !== null) {
            if (data.status == "1") {
                setMessage(`${entityNameContract} has been deleted successfully!`, null, `contract`);
                this.searchTable.bootstrapTable('refresh');
            } else {
                setMessage(`Error in deleting the ${entityNameContract} !`, 'danger', `contract`);
            }
        }
    };
    postRequest(pmgDeleteUrlContract, dataToDelete, processResult.bind(this));
}

pmgTableContract.deletePopUp = function (obj, row, index) {
    if (!variablesExist(['entityNameContract', 'pmgDeleteUrlContract'])) {
        return;
    }

    var dataRow = this.searchTable.bootstrapTable('getData')[index];

    var text = `Are you sure you want do delete this ${entityNameContract}?`;
    var buttons = [{
            text: "Yes",
            action: this.deleteData.bind(this, dataRow)
        }, {
            text: "No"
        }
    ];
    showModalMessage(text, buttons);
}

pmgTableContract.loadForm = function (obj, row, index, mode) {
    if (!variablesExist(['pmgFormPageContract', 'pmgFormParametersContract'])) {
        return;
    }

    let dataRow = this.searchTable.bootstrapTable('getData')[index];
    loadPage(pmgFormPageContract, pmgFormParametersContract(dataRow, mode));
}

pmgTableContract.renderUpdateActions = function (value, row, index) {
    var objectReturn = ['<div class="btn-group-vertical class="margin-top:1rem;margin-bottom:1rem">'];
    objectReturn.push(
            `<button 
            class="btn btn-primary" 
            onclick="pmgTableContract.loadForm(this,'${row}','${index}','view')" 
            title="View"
        >
            View
        </button>`
            );
    objectReturn.push(
            `<button 
                class="btn btn-primary"
                onclick="pmgTableContract.loadForm(this,'${row}','${index}','edit')" 
                title="Edit"
            >
                Edit
            </button>`,
            );
    if (!row.status) {
        row.status = "";
    }
    if (row.status.toLowerCase() == "draft" && row.isCreator == true) {
        objectReturn.push(
                `<button 
                class="btn btn-primary" 
                onclick="pmgTableContract.deletePopUp(this,'${row}','${index}')" 
                title="Delete" 
            >
                Delete
            </button>`,
                );
    }
    objectReturn.push('</div>');
    return objectReturn.join(' ');
};