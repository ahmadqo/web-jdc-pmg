pmgTableTask = {
    searchTable: {}
};

pmgTableTask.initialize = function () {
    var invalidSymbols = [
        "-",
        "+",
        "e",
    ];
    if (!variableExists('searchTableIdTask')) {
        return;
    }
    this.searchTable = $(`#${searchTableIdTask}`);
    this.searchTable.bootstrapTable();

    $('thead select.task').on('change', this.refresh);
    $('thead input.task').on('keypress', function (event) {
        if (event.which === 13) {
            event.preventDefault();
            this.refresh();
        }
    }.bind(this));

    $('thead .numbertask').on('keydown', function (event) {
        if (invalidSymbols.includes(event.key)) {
            event.preventDefault();
        }
    });
}

pmgTableTask.refresh = function () {
    pmgTableTask.searchTable.bootstrapTable('refresh');
    if (localStorage.projectUserRole == "PRG") {
        $('table .actionTable').hide();
    }
}

pmgTableTask.ajax = function (params) {
    if (!variablesExist(['searchTableUrlTask', 'searchParametersTask'])) {
        return;
    }
    var payload = searchParametersTask(params);
    postRequest(window.searchTableUrlTask, payload, function (result) {
        params.success(result);
    });
}

pmgTableTask.deleteData = function (dataToDelete) {
    if (!variablesExist(['entityNameTask', 'pmgDeleteUrlTask'])) {
        return;
    }

    if ('Task' === entityNameTask && dataToDelete.pic.toLowerCase() == localStorage.email.toLowerCase() && dataToDelete.statusTask != 'Completed') {
        let yourTask = localStorage.projectRemainingTask;
        if (yourTask > 0) {
            yourTask = yourTask - 1;
        }
        localStorage.setItem("projectRemainingTask", yourTask);
        document.getElementById('project-remainingTask').innerHTML = "Remaining Task: " + `<span class="badge">${yourTask}</span>`;
    }

    var processResult = function (data) {
        if (data !== null) {
            if (data.status == "1") {
                setMessage(`${entityNameTask} has been deleted successfully!`);
                this.searchTable.bootstrapTable('refresh');
            } else {
                setMessage(`Error in deleting the ${entityNameTask} !`, 'danger');
            }
        }
    };
    postRequest(pmgDeleteUrlTask, dataToDelete, processResult.bind(this));
}

pmgTableTask.deletePopUp = function (obj, row, index) {
    if (!variablesExist(['entityNameTask', 'pmgDeleteUrlTask'])) {
        return;
    }

    var dataRow = this.searchTable.bootstrapTable('getData')[index];

    var text = `Are you sure you want do delete this ${entityNameTask}?`;
    var buttons = [{
            text: "Yes",
            action: this.deleteData.bind(this, dataRow)
        }, {
            text: "No"
        }
    ];
    showModalMessage(text, buttons);
}

pmgTableTask.loadForm = function (obj, row, index, mode) {
    if (!variablesExist(['pmgFormPageTask', 'pmgFormParametersTask'])) {
        return;
    }

    let dataRow = this.searchTable.bootstrapTable('getData')[index];
    loadPage(pmgFormPageTask, pmgFormParametersTask(dataRow, mode));
}

pmgTableTask.loadFormInsert = function (obj, row, index, mode) {
    if (!variablesExist(['pmgFormInsertPage', 'pmgFormParametersTask'])) {
        return;
    }

    let dataRow = this.searchTable.bootstrapTable('getData')[index];
    loadPage(pmgFormInsertPage, pmgFormParametersTask(dataRow, mode));
}

pmgTableTask.loadUpdate = function (obj, row, index) {
    if (!variablesExist(['pmgFormPageTask', 'pmgFormParametersTask'])) {
        return;
    }

    let dataRow = this.searchTable.bootstrapTable('getData')[index];
    loadPage('JDCFND003', {idTask: dataRow.idTask, isTask: true});
}
