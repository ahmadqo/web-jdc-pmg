const sessionIsExpired = data => {
    if (data.status == 1) {
        return false;
    }
    if (!data.message) {
        return false;
    }
    var message = data.message;
    if (!message.authorization) {
        return false;
    }
    if (message.authorization.toLowerCase() != 'invalid request') {
        return false;
    }
    if (message.authentication.toLowerCase() != 'invalid request') {
        return false;
    }
    window.sessionTimeOut = true;
    if ('' == localStorage.username || null == localStorage.username) {
        localStorage.clear();
        document.cookie = "JDCID=;expires=Thu, 01 JAN 1970 00:00:01 GMT; path=/;";
        window.location = "login.html";
    } else {
        showSessionModal();
    }
    return true;
}

const showSessionModal = function () {
    console.log('Showing Session Modal');
    $('#modal-session').modal('show');
    $('#session-username').val(localStorage.username);
    $('#session-urlPhoto').val(localStorage.urlPhoto);
};

const imgError = function (image) {
    image.onerror = "";
    image.src = `https://ui-avatars.com/api/?length=3&color=000000&background=E9F1F3&name=${localStorage.fullname}`;
};

const reauthorize = function () {
    $('#session-error').hide();
    var user = $('#session-username').val();
    var password = $('#session-password').val();

    $('#alert-error').hide();
    $('#button-login').hide();
    $('#button-loading').show();

    let processResponse = function (response) {
        if (response.status == 1) {
            var message = response.message;
            localStorage.setItem("role", message.role);
            localStorage.setItem("npk", message.npk);
            localStorage.setItem("fullname", capitalize(message.fullname));
            localStorage.setItem("message", message.message);
            localStorage.setItem("email", message.email);
            localStorage.setItem("username", message.username);
            localStorage.setItem("JDCID", message.JDCID);
            localStorage.setItem("TKID", message.TKID);
            if (null == message.urlPhoto || '' == message.urlPhoto || 'null' == message.urlPhoto) {
                localStorage.setItem("urlPhoto", `https://ui-avatars.com/api/?length=3&color=000000&background=E9F1F3&name=${localStorage.fullname}`);
            } else {
                localStorage.setItem("urlPhoto", message.urlPhoto);
            }

            window.location.reload(true);
        } else {
            $('#session-error').show();
        }
    };
    var realm = 'JDC';
    var nonce = genRes(32);
    var uri = '/svc-jdc-dashboard/rest/jdc/dsh000/login';
    var cnonce = genRes(8);
    var nc = genNc(8);
    var qop = 'auth';
    var res = btoa(realm + ":" + password + ":" + nonce);
    
    var header = 'Digest username="' + user + '", realm="' + realm
            + '", nonce="' + nonce + '", uri="' + uri + '", cnonce="'
            + cnonce + '", nc="' + nc + '", qop="' + qop + '", response="'
            + res + '"';

    let ajaxOptions = {
        type: 'POST',
        headers: {
            Authorization: header
        },
        url: window.serviceUrl + '/svc-jdc-dashboard/rest/jdc/dsh000/login',
        contentType: 'application/json',
        dataType: 'json',
        async: false,
        success: processResponse
    };
    $.ajax(ajaxOptions);
};

function genNc(b) {
    var c = [], e = "0123456789", a = e.length;
    for (var d = 0; d < b; ++d) {
        c.push(e[Math.random() * a | 0])
    }
    return c.join("")
};

function genRes(b) {
    var c = [], e = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789", a = e.length;
    for (var d = 0; d < b; ++d) {
        c.push(e[Math.random() * a | 0])
    }
    return c.join("")
};