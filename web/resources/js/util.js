$.ajaxSetup({
    beforeSend: function (xhr, settings) {
        xhr.setRequestHeader("JDCID", localStorage.JDCID);
        xhr.setRequestHeader("TKID", localStorage.TKID);
        xhr.setRequestHeader("X-Frame-Options", 'DENY');
    }
});

/* General Functions */
var formatNull = function (value, row, index) {
    if (value == null || value == '') {
        return '-';
    }
    return value;
}

var formatCurrency = function (value, row, index) {
    if (value == null || value == '') {
        return '-';
    }
    return currencyConvert(value);
}

function currencyConvert(num) {
    return num.toFixed(0).replace(/\./, ",").replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1.");
}

var formatMoney = function (value, row, index) {
    if (value == null || value == '' || value == '0') {
        return '0';
    }
    return moneyConvert(value);
}

var formatEACMoneyHeader = function (value, row, index) {
    if (row['isFlag'] == true) {
        if (value == null || value == '') {
            return '<div class="eacheader">-</div>';
        } else {
            return '<div class="eacheader" style="font-weight:bold;">' + moneyConvert(value) + '</div>';
        }
    } else {
        if (value == null || value == '' || value == '0') {
            return '0';
        } else {
            return moneyConvert(value);
        }
    }
}

var formatRevenueHeader = function (value, row, index) {
    if (row['isFlag'] == true) {
        if (value == null || value == '') {
            return '<div class="revenueheader">-</div>';
        } else {
            return '<div class="revenueheader" style="font-weight:bold;">' + value + '</div>';
        }
    } else {
        if (value == null || value == '') {
            return '-';
        } else {
            return value;
        }
    }
}

var formatEACHeader = function (value, row, index) {
    if (row['isFlag'] == true) {
        if (value == null || value == '') {
            return '<div class="eacheader">-</div>';
        } else {
            return '<div class="eacheader" style="font-weight:bold;">' + value + '</div>';
        }
    } else {
        if (value == null || value == '') {
            return '-';
        } else {
            return value;
        }
    }
}

var formatFTLinkBudget = function (value, row, index) {
    return generateFTLinkContent(value, row, index, 'budget');
}

var formatFTLinkActual = function (value, row, index) {
    return generateFTLinkContent(value, row, index, 'actual');
}

var formatFTLinkCommit = function (value, row, index) {
    return generateFTLinkContent(value, row, index, 'commit');
}

function generateFTLinkContent(value, row, index, mode) {
    if (row['isFlag'] == true) {
        if (value == null || value == '' || value == '0') {
            return '<div class="eacheader">0</div>';
        } else {
            return '<div class="eacheader" onclick="fTlink(\'' + row['projectNumber'] + '\',\'' + row['wbs'] + '\',\'' + row['gl'] + '\',\'' + mode + '\')" style="font-weight:bold; cursor:pointer">' + moneyConvert(value) + '</div>';
        }
    } else {
        if (value == null || value == '' || value == '0') {
            return '0';
        } else {
            return '<div onclick="fTlink(\'' + row['projectNumber'] + '\',\'' + row['wbs'] + '\',\'' + row['gl'] + '\',\'' + mode + '\')"  style="cursor:pointer">' + moneyConvert(value) + '</div>';
        }
    }
}

var formatFTstatus = function (value, row, index) {
    if (row['isFlag'] == true) {
        if (value == null || value == '') {
            return '<div class="eacheader">-</div>';
        } else {
            if (value == 'On Budget') {
                return '<div class="eacheader statusok" style="font-weight:bold;">' + value + '</div>';
            } else if (value == 'Alert') {
                return '<div class="eacheader statuswarning" style="font-weight:bold;">' + value + '</div>';
            } else if (value == 'Over') {
                return '<div class="eacheader statusdanger" style="font-weight:bold;">' + value + '</div>';
            } else {
                return '<div class="eacheader" style="font-weight:bold;">' + value + '</div>';
            }
        }
    } else {
        if (value == null || value == '') {
            return '-';
        } else {
            if (value == 'On Budget') {
                return '<div class="statusok">' + value + '</div>';
            } else if (value == 'Alert') {
                return '<div class="statuswarning">' + value + '</div>';
            } else if (value == 'Over') {
                return '<div class="statusdanger">' + value + '</div>';
            } else {
                return value;
            }
        }
    }
}

var counter = 0;
var ids = 0;
var formatFTeditable = function (value, row, index) {
    if (localStorage.projectUserRole == 'PM' || localStorage.projectUserRole == 'OM') {
        if (row['isFlag'] == true) {
            counter++;
            if (value == null || value == '') {
                return '<div class="eacheader ftetchead-' + counter + '">0<div>';
            } else {
                return '<div class="eacheader ftetchead-' + counter + '" style="font-weight:bold;">' + moneyConvert(value) + '</div>';
            }
        } else {
            ids++;
            if (value == null || value == '' || value == '0') {
                return '<input id="ftetcid-' + ids + '" class="form-control ftetcs ftetc-' + counter + '" type="number" value=0   onchange="ftetcediting(\'' + row['projectNumber'] + '\',\'' + row['wbs'] + '\',\'' + row['gl'] + '\',\'ftetcid-' + ids + '\', this.value)">';
            } else {
                return '<input id="ftetcid-' + ids + '" class="form-control ftetcs ftetc-' + counter + '" type="number" value="' + value + '" onchange="ftetcediting(\'' + row['projectNumber'] + '\',\'' + row['wbs'] + '\',\'' + row['gl'] + '\',\'ftetcid-' + ids + '\', this.value)">';
            }
        }
    } else {
        if (row['isFlag'] == true) {
            if (value == null || value == '') {
                return '<div class="eacheader">-</div>';
            } else {
                return '<div class="eacheader" style="font-weight:bold;">' + moneyConvert(value) + '</div>';
            }
        } else {
            if (value == null || value == '' || value == '0') {
                return '0';
            } else {
                return moneyConvert(value);
            }
        }
    }
}

function moneyConvert(n, c, d, t) {
    var c = isNaN(c = Math.abs(c)) ? 0 : c,
            d = d == undefined ? "," : d,
            t = t == undefined ? "." : t,
            s = n < 0 ? "-" : "",
            i = String(parseInt(n = Math.abs(Number(n) || 0).toFixed(c))),
            j = (j = i.length) > 3 ? j % 3 : 0;

    return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(0) : "");
}
;

const setMessage = function (message, level, module) {
    if (!level) {
        level = 'success'
    }
    var messageHtml =
        `<div class="alert alert-${level} alert-dismissable fade in">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <strong>${message}</strong>
        </div>`;
    $('.alert-dismissable').remove();

    if (module) {
        $('section.content' + module).prepend(messageHtml);
    } else {
        $('section.content').prepend(messageHtml);
    }
}

const postRequest = function (url, data, success, options) {
    if (!window.sessionTimeOut) {
        window.sessionTimeOut = false;
    }
    if (window.sessionTimeOut) {
        return;
    }
    if (!options) {
        options = {};
    }
    var successWrapper = response => {
        if (sessionIsExpired(response)) {
            return;
        }
        if (response.status == 0) {
            if (!response.message) {
                return;
            }
            if (!response.message.message) {
                return;
            }
            var messages = response.message.message;

            if (!messages.forEach) {
                console.warn('The message is not an array list');
                messages = [messages];
            }

            var messageText = '<p>Information :<p> <ul>';
            messages.forEach(message => {
                messageText += `<li>${message}</li>`
            });

            messageText += '</ul>'
            showModalMessage(messageText);

            if (options.performSuccessAnyway && success) {
                success(response);
            } else {
                return;
            }
        }

        if (success) {
            success(response);
        }
    }

    if (!variableExists('serviceUrl')) {
        return;
    }

    var ajaxOptions = {
        type: 'POST',
        url: window.serviceUrl + url,
        contentType: 'application/json',
        dataType: 'json',
        async: false,
        success: successWrapper
    };

    if (!variableExists('serviceUrl')) {
        return;
    }

    if (options != null) {
        for (property in options) {
            ajaxOptions[property] = options[property];
        }
        if (options.sameOrigin) {
            ajaxOptions.url = url;
            delete options.sameOrigin;
        }
    }

    if (data) {
        if (typeof data == 'object') {
            data = JSON.stringify(data);
        }
        ajaxOptions.data = data;
    }
    $.ajax(ajaxOptions);
}

const loadMemberBox = function (parameter, element) {
    var processResult = function (res) {
        var email = 'email';
//        debugger;
        if (res.data) {
            var data = res.data;
            data = data.map(datum => `<label><input type="checkbox" id="email" value="${datum[email]}"/>${datum[email]}</label><br/>`)
            $(element).html(data);
        }
    }
    var url = '/svc-jdc-project-management/rest/jdc/pmg000/get-data-member-organization';
    postRequest(url, parameter, processResult);
};

const loadSelect = function (selectId, selectName, selectElement, options) {
    var processResult = function (res) {
        var optionHtml = `<option value="">-- Select ${selectName} --</option>`;
        var valueField = 'commonkey';
        var textField = 'commdesc1';

        if (options) {
            if (options.valueField) {
                valueField = options.valueField;
            }
            if (options.textField) {
                textField = options.textField;
            }
        }
        if (res.data) {
            var data = res.data;
            data = data.map(datum => `<option value="${datum[valueField].toString()}">${datum[textField]}</option>`);
            optionHtml += data.join('');
            $(selectElement).html(optionHtml);
        }
    }
    var url = '/svc-jdc-foundation/rest/jdc/fnd001/get-data-settings';
    postRequest(url, selectId, processResult, options);
};

const loadSelectPic = function (selectId, selectName, selectElement, options) {
    var processResult = function (res) {
        var optionHtml = `<option value="">-- Select ${selectName} --</option>`;
        var valueField = 'commonkey';
        var textField = 'commdesc1';

        if (options) {
            if (options.valueField) {
                valueField = options.valueField;
            }
            if (options.textField) {
                textField = options.textField;
            }
        }
        if (res.data) {
            var data = res.data;
            data = data.map(datum => `<option value="${datum[valueField].toString().toLowerCase()}">${datum[textField]}</option>`);
            optionHtml += data.join('');
            $(selectElement).html(optionHtml);
        }
    }
    var url = '/svc-jdc-foundation/rest/jdc/fnd001/get-data-settings';
    postRequest(url, selectId, processResult, options);
};

const loadSelectValue = function (selectId, selectName, selectElement, selectField, options) {
    var processResult = function (res) {
        var optionHtml = `<option value="">-- Select ${selectName} --</option>`;
        var valueField = selectField;
        var textField = selectField;
        if (options) {
            if (options.valueField) {
                valueField = options.valueField;
            }
            if (options.textField) {
                textField = options.textField;
            }
        }

        if (res.data) {
            var data = res.data;
            data = data.map(datum => `<option value="${datum[valueField]}">${datum[textField]}</option>`);
            optionHtml += data.join('');
            $(selectElement).html(optionHtml);
        }
        ;
    };

    var url = '/svc-jdc-foundation/rest/jdc/fnd001/get-data-settings';

    postRequest(url, selectId, processResult, options);
};

const loadSelectAllValue = function (selectId, selectName, selectElement, selectField, options) {
    var processResult = function (res) {
        var optionHtml = `<option value="">${selectName}</option>`;
        var valueField = selectField;
        var textField = selectField;
        if (options) {
            if (options.valueField) {
                valueField = options.valueField;
            }
            if (options.textField) {
                textField = options.textField;
            }
        }

        if (res.data) {
            var data = res.data;
            data = data.map(datum => `<option value="${datum[valueField]}">${datum[textField]}</option>`);
            optionHtml += data.join('');
            $(selectElement).html(optionHtml);
        }
        ;
    };

    var url = '/svc-jdc-foundation/rest/jdc/fnd001/get-data-settings';

    postRequest(url, selectId, processResult, options);
};

/* User stuffs */

const renderUserDetail = function () {
    $(".user-fullname").text(localStorage.fullname);
    $(".user-npk").text(localStorage.npk);
    $(".user-avatar").attr("src", localStorage.urlPhoto);
};

var logout = function () {
    let processResponse = response => {
        if (response.status == "1") {
            localStorage.clear();
            document.cookie = "JDCID=;expires=Thu, 01 JAN 1970 00:00:01 GMT; path=/;";
            window.location = "login.html";
        }
    };

    let url = "/svc-jdc-dashboard/rest/jdc/dsh000/logout";

    var ajaxOptions = {
        type: 'POST',
        url: window.serviceUrl + url,
        contentType: 'application/json',
        dataType: 'json',
        async: false,
        success: processResponse
    };

    $.ajax(ajaxOptions);


}

const findBootstrapEnvironment = function () {
    var envs = ['xs', 'sm', 'md', 'lg'];

    var $el = $('<div>');
    $el.appendTo($('body'));

    for (var i = envs.length - 1; i >= 0; i--) {
        var env = envs[i];

        $el.addClass('hidden-' + env);
        if ($el.is(':hidden')) {
            $el.remove();
            return env;
        }
    }
}

var projectTypes = {
    'PM': '001',
    'PRJ': '001',
    'PRG': '001',
    'OM': '002',
    'OPR': '002',
    'DP': '002'
};

var getProjectType = function () {
    if (!localStorage.projectUserRole) {
        return pageParameters.projectType;
    } else {
        return projectTypes[localStorage.projectUserRole];
    }
}

var showModalMessage = function (text, buttons, action) {
    var defaultAction = function () {
        $('.modal-backdrop').remove();
        $('body').removeClass('modal-open');
        $('body').css('padding-right', '0');
        $('#modal-message').modal('hide');


    };
    if (!buttons) {
        buttons = [{text: 'OK'}];
    }
    $('#modal-message-text').html(text);
    var modalMessageButtonsElement = $('#modal-message-buttons');
    modalMessageButtonsElement.empty();
    buttons.forEach(button => {
        var buttonElement = $(`<button type="button" class="btn btn-default btn-primary large-button"/>`);
        buttonElement.text(button.text);
        if (button.action) {
            buttonElement.on('click', function () {
                defaultAction();
                button.action();
            });
        } else {
            buttonElement.on('click', defaultAction);
        }
        modalMessageButtonsElement.append(buttonElement);
    });
    $('#modal-message').modal('show');
};

var variableExists = function (variableName, object) {
    if (!object) {
        object = window;
    }
    if (object[variableName]) {
        return true;
    } else {
        console.error(`Can't find ${variableName}`);
        return false;
    }
}

var variablesExist = function (variableNames, object) {
    if (!object) {
        object = window;
    }
    if (variableNames.forEach) {
        var result = true;
        variableNames.forEach(variableName => {
            if (!variableExists(variableName, object)) {
                result = false;
            }
        });
        return result;
    } else {
        return variableExists(variableNames, object);
    }
};

var elementExists = function (selector) {
    return $(selector).length > 0;
}

var elementsExist = function (selectors) {
    if (selectors.forEach) {
        var result = true;
        selectors.forEach(selector => {
            if (elementExists(selector)) {
                result = false;
            }
        });
        return result;
    } else {
        return elementExists(selectors);
    }
};

const linkTwoDatePickers = function (startId, endId) {

    var startPicker = $(`#${startId}`);
    var endPicker = $(`#${endId}`);

    startPicker.on("dp.change", function (e) {
        if (e.date === null || e.date === '') {
            endPicker.data("DateTimePicker").minDate(new Date(-8640000000000000));
        } else {
            endPicker.data("DateTimePicker").minDate(e.date);
        }
    });

    endPicker.on("dp.change", function (e) {
        if (e.date === null || e.date === '') {
            startPicker.data("DateTimePicker").maxDate(new Date(8640000000000000));
        } else {
            startPicker.data("DateTimePicker").maxDate(e.date);
        }
    });
}

var dateUtil = {
    convert: function (d) {
        // Converts the date in d to a date-object. The input can be:
        //   a date object: returned without modification
        //  an array      : Interpreted as [year,month,day]. NOTE: month is 0-11.
        //   a number     : Interpreted as number of milliseconds
        //                  since 1 Jan 1970 (a timestamp) 
        //   a string     : Any format supported by the javascript engine, like
        //                  "YYYY/MM/DD", "MM/DD/YYYY", "Jan 31 2009" etc.
        //  an object     : Interpreted as an object with year, month and date
        //                  attributes.  **NOTE** month is 0-11.
        return (
                d.constructor === Date ? d :
                d.constructor === Array ? new Date(d[0], d[1], d[2]) :
                d.constructor === Number ? new Date(d) :
                d.constructor === String ? new Date(d) :
                typeof d === "object" ? new Date(d.year, d.month, d.date) :
                NaN
                );
    },
    compare: function (a, b) {
        // Compare two dates (could be of any type supported by the convert
        // function above) and returns:
        //  -1 : if a < b
        //   0 : if a = b
        //   1 : if a > b
        // NaN : if a or b is an illegal date
        // NOTE: The code inside isFinite does an assignment (=).
        return (
                isFinite(a = this.convert(a).valueOf()) &&
                isFinite(b = this.convert(b).valueOf()) ?
                (a > b) - (a < b) :
                NaN
                );
    },
    inRange: function (d, start, end) {
        // Checks if date in d is between dates in start and end.
        // Returns a boolean or NaN:
        //    true  : if d is between start and end (inclusive)
        //    false : if d is before start or after end
        //    NaN   : if one or more of the dates is illegal.
        // NOTE: The code inside isFinite does an assignment (=).
        return (
                isFinite(d = this.convert(d).valueOf()) &&
                isFinite(start = this.convert(start).valueOf()) &&
                isFinite(end = this.convert(end).valueOf()) ?
                start <= d && d <= end :
                NaN
                );
    }
}

var hideForRoleReadOnly = function () {
    if (localStorage.projectUserRole != 'PM' && localStorage.projectUserRole != 'OM') {
        $('.large-button').hide();
        $('.form-control').prop("disabled", true);
        $('.button-save').hide();
        $('.btnForAction').hide();
//        $('table .actionTable').hide();
        $('button[title="Edit"]').hide();
        $('button[title="Delete"]').hide();
        $('.button-info').show();
    }
}

function capitalize(s) {
    return s.toLowerCase().replace(/\b./g, function (a) {
        return a.toUpperCase();
    });
}
;
