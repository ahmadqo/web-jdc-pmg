pmgTableScurve = {
    searchTable: {}
};

pmgTableScurve.initialize = function () {
    var invalidSymbols = [
        "-",
        "+",
        "e",
    ];
    if (!variableExists('searchTableIdScurve')) {
        return;
    }
    this.searchTable = $(`#${searchTableIdScurve}`);
    this.searchTable.bootstrapTable();

    $('thead select.scurve').on('change', this.refresh);
    $('thead input.scurve').on('keypress', function (event) {
        if (event.which === 13) {
            event.preventDefault();
            this.refresh();
        }
    }.bind(this));

    $('thead .numberscurve').on('keydown', function (event) {
        if (invalidSymbols.includes(event.key)) {
            event.preventDefault();
        }
    });
}

pmgTableScurve.refresh = function () {
    pmgTableScurve.searchTable.bootstrapTable('refresh');
    if (localStorage.projectUserRole == "PRG") {
        $('table .actionTable').hide();
    }
}

pmgTableScurve.ajax = function (params) {
    if (!variablesExist(['searchTableUrlScurve', 'searchParametersScurve'])) {
        return;
    }
    var payload = searchParametersScurve(params);
    postRequest(window.searchTableUrlScurve, payload, function (result) {
        params.success(result);
    });
}

pmgTableScurve.deleteData = function (dataToDelete) {
    if (!variablesExist(['entityNameScurve', 'pmgDeleteUrlScurve'])) {
        return;
    }

    var processResult = function (data) {
        if (data !== null) {
            if (data.status == "1") {
                setMessage(`${entityNameScurve} has been deleted successfully!`, null, 'scurve');
                this.searchTable.bootstrapTable('refresh');
            } else {
                setMessage(`Error in deleting the ${entityNameScurve} !`, 'danger', 'scurve');
            }
        }
    };
    postRequest(pmgDeleteUrlScurve, dataToDelete, processResult.bind(this));
}

pmgTableScurve.deletePopUp = function (obj, row, index) {
    if (!variablesExist(['entityNameScurve', 'pmgDeleteUrlScurve'])) {
        return;
    }

    var dataRow = this.searchTable.bootstrapTable('getData')[index];

    var text = `Are you sure you want do delete this ${entityNameScurve}?`;
    var buttons = [{
            text: "Yes",
            action: this.deleteData.bind(this, dataRow)
        }, {
            text: "No"
        }
    ];
    showModalMessage(text, buttons);
}

pmgTableScurve.loadForm = function (obj, row, index, mode) {
    if (!variablesExist(['pmgFormPageScurve', 'pmgFormParametersScurve'])) {
        return;
    }

    let dataRow = this.searchTable.bootstrapTable('getData')[index];
    loadPage(pmgFormPageScurve, pmgFormParametersScurve(dataRow, mode));
}

pmgTableScurve.renderUpdateActions = function (value, row, index) {
    var objectReturn = ['<div class="btn-group-vertical class="margin-top:1rem;margin-bottom:1rem">'];
    objectReturn.push(
            `<button 
            class="btn btn-primary" 
            onclick="pmgTableScurve.loadForm(this,'${row}','${index}','view')" 
            title="View"
        >
            View
        </button>`
            );
    objectReturn.push(
            `<button 
                class="btn btn-primary"
                onclick="pmgTableScurve.loadForm(this,'${row}','${index}','edit')" 
                title="Edit"
            >
                Edit
            </button>`,
            );
    if (!row.status) {
        row.status = "";
    }
    if (row.status.toLowerCase() == "draft" && row.isCreator == true) {
        objectReturn.push(
                `<button 
                class="btn btn-primary" 
                onclick="pmgTableScurve.deletePopUp(this,'${row}','${index}')" 
                title="Delete" 
            >
                Delete
            </button>`,
                );
    }
    objectReturn.push('</div>');
    return objectReturn.join(' ');
};