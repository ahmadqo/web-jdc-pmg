pmgTableScope = {
    searchTable: {}
};

pmgTableScope.initialize = function () {
    var invalidSymbols = [
        "-",
        "+",
        "e",
    ];
    if (!variableExists('searchTableIdScope')) {
        return;
    }
    this.searchTable = $(`#${searchTableIdScope}`);
    this.searchTable.bootstrapTable();

    $('thead select.scope').on('change', this.refresh);
    $('thead input.scope').on('keypress', function (event) {
        if (event.which === 13) {
            event.preventDefault();
            this.refresh();
        }
    }.bind(this));

    $('thead .numberscope').on('keydown', function (event) {
        if (invalidSymbols.includes(event.key)) {
            event.preventDefault();
        }
    });
}

pmgTableScope.refresh = function () {
    pmgTableScope.searchTable.bootstrapTable('refresh');
}

pmgTableScope.ajax = function (params) {
    if (!variablesExist(['searchTableUrlScope', 'searchParametersScope'])) {
        return;
    }
    var payload = searchParametersScope(params);
    postRequest(window.searchTableUrlScope, payload, function (result) {
        params.success(result);
    });
}

pmgTableScope.deleteData = function (dataToDelete) {
    if (!variablesExist(['entityNameScope', 'pmgDeleteUrlScope'])) {
        return;
    }

    var processResult = function (data) {
        $("#loader-submit").show();
        if (data !== null) {
            if (data.status == "1") {
                setMessage(`${entityNameScope} has been deleted successfully!`, 'success', `scope`);
                this.searchTable.bootstrapTable('refresh');
                stopPreload();
                location.reload(true);
            } else {
                setMessage(`Error in deleting the ${entityNameScope} !`, 'danger', `scope`);
                stopPreload();
            }
        }
    };
    postRequest(pmgDeleteUrlScope, dataToDelete, processResult.bind(this));
}

pmgTableScope.deletePopUp = function (obj, row, index) {
    if (!variablesExist(['entityNameScope', 'pmgDeleteUrlScope'])) {
        return;
    }

    var dataRow = this.searchTable.bootstrapTable('getData')[index];

    var text = `Are you sure you want do delete this ${entityNameScope}?`;
    var buttons = [{
            text: "Yes",
            action: this.deleteData.bind(this, dataRow)
        }, {
            text: "No"
        }
    ];
    showModalMessage(text, buttons);
}

pmgTableScope.loadForm = function (obj, row, index, mode) {
    if (!variablesExist(['pmgFormPageScope', 'pmgFormParametersScope'])) {
        return;
    }

    let dataRow = this.searchTable.bootstrapTable('getData')[index];
    loadPage(pmgFormPageScope, pmgFormParametersScope(dataRow, mode));
}

pmgTableScope.renderActions = function (value, row, index) {
    var objectReturn = ['<div class="btn-group-vertical class="margin-top:1rem;margin-bottom:1rem">'];
    objectReturn.push(
        `<a id="jdcpmg000p01-downloadDocumntCheckList-${index}" class="btn btn-primary" href="" download>Download</a>`
    );
    objectReturn.push(
        `<button 
            class="btn btn-primary" 
            onclick="pmgTableScope.deletePopUp(this,'${row}','${index}')" 
            title="Delete" 
        >
        Delete
        </button>`,
    );
    objectReturn.push('</div>');
    return objectReturn.join(' ');
};

pmgTableScope.renderActionsOPR = function (value, row, index) {
    var objectReturn = ['<div class="btn-group-vertical style="margin-top:1rem;margin-bottom:1rem">'];
    objectReturn.push(
        `<a id="jdcpmg000p02-downloadDocumntCheckList-${index}" class="btn btn-primary" href="" download>Download</a>`
    );
    objectReturn.push(
        `<button 
            class="btn btn-primary btnActionDelete"
            onclick="pmgTableScope.deletePopUp(this,'${row}','${index}')" 
            title="Delete" 
        >
        Delete
        </button>`,
    );
    objectReturn.push('</div>');
    return objectReturn.join(' ');
};
