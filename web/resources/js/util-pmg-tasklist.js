const pmgTaskList = {
    data: [],
    url: {
        search: "/svc-jdc-project-management/rest/jdc/pmg000/search-task-list"
    }
};

pmgTaskList.render = function (result) {
    this.data = [];

    if (result.data) {
        this.data = result.data;
    }

    let length = this.data.length;

    let taskListContainer = $('#tasklist-items');
    taskListContainer.empty();

    if (length == 0) {
        $('#tasklist-button .label-warning').remove();
        return;
    }

    let shownData = this.data;

    if (length > 5) {
        shownData = shownData.slice(0, 5);
    }

    //Adding badge
    $('#tasklist-button').append(`<span class="label label-warning">${length}</span>`);


    //Adding upper message
    let message = `You have ${length} notification`;
    if (length > 1) {
        message += "s";
    }
    taskListContainer.append(`<li class="header">${message}</li>`);

    //Rendering Tasks
    let taskListElement = $(`<li><ul class="menu"></ul></li>`).appendTo(taskListContainer);
    let taskListMenu = taskListElement.find('.menu');

    shownData.forEach(shownDatum => {
        let taskItemElement = $('<li></li>').appendTo(taskListMenu);
        let taskItemLinkClass = shownDatum.isRead ? 'task-read' : 'task-unread';
        let taskItemLink = $(`
        <a href="#" class="${taskItemLinkClass}">
            <i class="fa fa-circle-o text-aqua"></i>${shownDatum.taskDesc}
        </a>`).appendTo(taskItemElement);
        taskItemLink.on('click', loadPage.bind(this, 'JDCFND003', {idTask: shownDatum.idTask}));
    });

    taskListElement = $('<li class="footer"></li>').appendTo(taskListContainer);

    //Adding View All
    var taskListFooter = $('<a href="#">View All</a>').appendTo(taskListElement);
    taskListFooter.on('click', loadPage.bind(this, 'JDCFND003', null));
    taskListContainer.append(taskListElement);
}

pmgTaskList.load = function () {
    let data = {search: {"=email": localStorage.email}};
    postRequest(this.url.search, data, this.render);
}
