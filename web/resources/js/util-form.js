const fillFormFields = function (data, prefix) {
    if (data.data == null) {
        console.log('Data is Null');
        return;
    }
    data = data.data
    if (data.length == 0) {
        console.log('Data is empty');
        return;
    }
    data = data[0];
    for (var property in data) {
        $(`#${prefix}-${property}`).val(data[property]);
    }

    let data2 = data.contractSummary;
    for (var property2 in data2) {
        $(`#${prefix}-${property2}`).val(data2[property2]);
    }
};


const createFormObject = function (prefix, formFields, idField) {
    var formObject = {};

    formFields.forEach(formField => {
        formObject[formField] = $(`#${prefix}-${formField}`).val();
    });

    if ($(`#${prefix}-${idField}`).val()) {
        formObject[idField] = $(`#${prefix}-${idField}`).val();
    }
    return formObject;
}

const validateForm = function (formSelector) {
    var validated = true;
    var formElement = $(formSelector);
    var requiredFields = formElement.find('.required');
    var validateField = function (index, element) {
        element = $(element);
        if (element.val() == '' || element.val() == null) {
            validated = false;
            element.addClass('is-empty');
        }
    }
    requiredFields.removeClass('is-empty');
    requiredFields.each(validateField);
    return validated;
}