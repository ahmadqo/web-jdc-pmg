pmgTableBast = {
    searchTable: {}
};

pmgTableBast.initialize = function () {
    var invalidSymbols = [
        "-",
        "+",
        "e",
    ];
    if (!variableExists('searchTableIdBast')) {
        return;
    }
    this.searchTable = $(`#${searchTableIdBast}`);
    this.searchTable.bootstrapTable();

    $('thead select.bast').on('change', this.refresh);
    $('thead input.bast').on('keypress', function (event) {
        if (event.which === 13) {
            event.preventDefault();
            this.refresh();
        }
    }.bind(this));

    $('thead .numberbast').on('keydown', function (event) {
        if (invalidSymbols.includes(event.key)) {
            event.preventDefault();
        }
    });
}

pmgTableBast.refresh = function () {
    pmgTableBast.searchTable.bootstrapTable('refresh');
    if (["PRG", "DP"].indexOf(localStorage.projectUserRole) > -1) {
        $('table .actionTable').hide();
    }
}

pmgTableBast.ajax = function (params) {
    if (!variablesExist(['searchTableUrlBast', 'searchParametersBast'])) {
        return;
    }
    var payload = searchParametersBast(params);
    postRequest(window.searchTableUrlBast, payload, function (result) {
        params.success(result);
    });
}

pmgTableBast.deleteData = function (dataToDelete) {
    if (!variablesExist(['entityNameBast', 'pmgDeleteUrlBast'])) {
        return;
    }

    var processResult = function (data) {
        if (data !== null) {
            if (data.status == "1") {
                setMessage(`${entityNameBast} has been deleted successfully!`, null, `bast`);
                this.searchTable.bootstrapTable('refresh');
            } else {
                setMessage(`Error in deleting the ${entityNameBast} !`, 'danger', `bast`);
            }
        }
    };
    postRequest(pmgDeleteUrlBast, dataToDelete, processResult.bind(this));
}

pmgTableBast.deletePopUp = function (obj, row, index) {
    if (!variablesExist(['entityNameBast', 'pmgDeleteUrlBast'])) {
        return;
    }

    var dataRow = this.searchTable.bootstrapTable('getData')[index];

    var text = `Are you sure you want do delete this ${entityNameBast}?`;
    var buttons = [{
            text: "Yes",
            action: this.deleteData.bind(this, dataRow)
        }, {
           text: "No"
        }
    ];
    showModalMessage(text, buttons);
}

pmgTableBast.loadForm = function (obj, row, index, mode) {
    if (!variablesExist(['pmgFormPageBast', 'pmgFormParametersBast'])) {
        return;
    }

    let dataRow = this.searchTable.bootstrapTable('getData')[index];
    loadPage(pmgFormPageBast, pmgFormParametersBast(dataRow, mode));
}

pmgTableBast.renderUpdateActions = function (value, row, index) {
    var objectReturn = ['<div class="btn-group-vertical class="margin-top:1rem;margin-bottom:1rem">'];
    objectReturn.push(
            `<button 
            class="btn btn-primary" 
            onclick="pmgTableBast.loadForm(this,'${row}','${index}','view')" 
            title="View">
            View
        </button>`
            );
    objectReturn.push(
            `<button 
                class="btn btn-primary"
                onclick="pmgTableBast.loadForm(this,'${row}','${index}','edit')" 
                title="Edit">
                Edit
            </button>`,
            );
    if (!row.status) {
        row.status = "";
    }
    if (row.status.toLowerCase() == "draft" && row.isCreator == true) {
        objectReturn.push(
                `<button 
                class="btn btn-primary" 
                onclick="pmgTableBast.deletePopUp(this,'${row}','${index}')" 
                title="Delete" 
            >
                Delete
            </button>`,
                );
    }
    objectReturn.push('</div>');
    return objectReturn.join(' ');
};